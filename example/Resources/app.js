(function() {
  var rcv = 0;
  var ws;

  function testSend() {
    Ti.API.debug("sending", rcv);
    ws.send("hey");
  }

  ws = require("cat.start.tiws").createWebSocket({
    uri: "wss://echo.websocket.org",
    connectionTimeout: 5, // seconds
    pingInterval: 2, // seconds
    headers: {
      // both key and value must be strings
      "x-iay-hellow": "yellow"
    },
    onOpen: function() {
      Ti.API.debug("onOpen");

      testSend();
    },
    onError: function(ev) {
      // this only could happen if cannot connect for some reason
      Ti.API.debug("onError", ev.error);
    },
    onMessage: function(ev) {
      rcv += 1;
      Ti.API.debug("onMessage", ev.data, rcv);

      if (rcv < 5) {
        setTimeout(testSend, 5000);
      } else {
        Ti.API.debug("closing");
        ws.close();
      }
    },
    onClose: function(ev) {
      Ti.API.debug("onClose", ev); // ev.error
    }
  });
})();
