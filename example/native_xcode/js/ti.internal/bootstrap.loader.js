/**
 * Appcelerator Titanium Mobile
 * Copyright (c) 2018 by Axway, Inc. All Rights Reserved.
 * Licensed under the terms of the Apache Public License
 * Please see the LICENSE included with this distribution for details.
 *
 * Description:
 * This script loads all JavaScript files ending with the name "*.bootstrap.js" and then executes them.
 * The main intention of this feature is to allow JavaScript files to kick-off functionality or
 * display UI to the end-user before the "app.js" gets loaded. This feature is the CommonJS
 * equivalent to Titanium's Android module onAppCreate() or iOS module load() features.
 *
 * Use-Cases:
 * - Automatically kick-off analytics functionality on app startup.
 * - Ensure "Google Play Services" is installed/updated on app startup on Android.
 */
'use strict';
/**
               * Attempts to load all bootstraps from a "bootstrap.json" file created by the app build system.
               * This is an optional feature and is the fastest method of acquiring boostraps configured for the app.
               * This JSON file, if provided, must be in the same directory as this script.
               * @returns {Array.<string>}
               * Returns an array of require() compatible strings if bootstraps were successfully loaded from JSON.
               * Returns an empty array if JSON file was found, but no bootstraps were configured for the app.
               * Returns null if JSON file was not found.
               */

function fetchScriptsFromJson() {
  var JSON_FILE_NAME = 'bootstrap.json',
  jsonFile,
  settings;

  try {
    jsonFile = Ti.Filesystem.getFile(Ti.Filesystem.resourcesDirectory, 'ti.internal/' + JSON_FILE_NAME);

    if (jsonFile.exists()) {
      settings = JSON.parse(jsonFile.read().text);

      if (Array.isArray(settings.scripts)) {
        return settings.scripts;
      } else {
        return [];
      }
    }
  } catch (error) {
    Ti.API.error('Failed to read "' + JSON_FILE_NAME + '". Reason: ' + error.message);
  }

  return null;
}
/**
   * Recursively searches the "Resources" directory for all "*.bootstrap.js" files.
   * @returns {Array.<string>}
   * Returns an array of require() compatible strings for each bootstrap found in the search.
   * Returns an empty array if no bootstrap files were found.
   */


function fetchScriptsFromResourcesDirectory() {
  var resourceDirectory = Ti.Filesystem.getFile(Ti.Filesystem.resourcesDirectory),
  resourceDirectoryPath = resourceDirectory.nativePath,
  bootstrapScripts = [];

  function loadFrom(file) {
    var index, fileNameArray, bootstrapPath;

    if (file) {
      if (file.isDirectory()) {
        // This is a directory. Recursively look for bootstrap files under it.
        fileNameArray = file.getDirectoryListing();

        if (fileNameArray) {
          for (index = 0; index < fileNameArray.length; index++) {
            loadFrom(Ti.Filesystem.getFile(file.nativePath, fileNameArray[index]));
          }
        }
      } else if (file.name.search(/.bootstrap.js$/) >= 0) {
        // This is a bootstrap file.
        // Convert its path to something loadable via require() and add it to the array.
        bootstrapPath = file.nativePath;
        bootstrapPath = bootstrapPath.substr(resourceDirectoryPath.length, bootstrapPath.length - resourceDirectoryPath.length - '.js'.length);
        bootstrapScripts.push(bootstrapPath);
      }
    }
  }

  loadFrom(resourceDirectory);
  return bootstrapScripts;
}
/**
   * Non-blocking function which loads and executes all bootstrap scripts configured for the app.
   * @param {function} finished Callback to be invoked once all bootstraps have finished executing. Cannot be null.
   */


exports.loadAsync = function (finished) {
  // Acquire an array of all bootstrap scripts included with the app.
  // - For best performance, attempt to fetch scripts via an optional JSON file create by the build system.
  // - If JSON file not found (will return null), then search "Resources" directory for bootstrap files.
  var bootstrapScripts = fetchScriptsFromJson();

  if (!bootstrapScripts) {
    bootstrapScripts = fetchScriptsFromResourcesDirectory();
  } // Do not continue if no bootstraps were found.


  if (!bootstrapScripts || bootstrapScripts.length <= 0) {
    finished();
    return;
  } // Sort the bootstraps so that they'll be loaded in a consistent order between platforms.


  bootstrapScripts.sort(); // Loads all bootstrap scripts found.

  function loadBootstrapScripts(finished) {
    var bootstrapIndex = 0;

    function doLoad() {
      // Attempt to load all bootstrap scripts.
      var fileName, bootstrap;

      while (bootstrapIndex < bootstrapScripts.length) {
        // Load the next bootstrap.
        fileName = bootstrapScripts[bootstrapIndex];
        bootstrap = require(fileName); // Invoke the bootstrap's execute() method if it has one. (This is optional.)
        // We must wait for the given callback to be invoked before loading the next script.
        // Note: This is expected to be used to display UI to the end-user.

        if (bootstrap.execute) {
          bootstrap.execute(onBootstrapExecutionFinished);
          return;
        } // We're done with the current bootstrap. Time to load the next one.


        bootstrapIndex++;
      } // Invoke given callback to inform caller that all loading is done.


      finished();
    }

    function onBootstrapExecutionFinished() {
      // Last bootstrap has finished execution. Time to load the next one.
      // Note: Add a tiny delay so whatever UI the last bootstrap loaded has time to close.
      bootstrapIndex++;
      setTimeout(function () {
        doLoad();
      }, 1);
    }

    doLoad();
  }

  loadBootstrapScripts(function () {
    // We've finished loading/executing all bootstrap scripts.
    // Inform caller by invoking the callback given to loadAsync().
    finished();
  });
};
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImJvb3RzdHJhcC5sb2FkZXIuanMiXSwibmFtZXMiOlsiZmV0Y2hTY3JpcHRzRnJvbUpzb24iLCJKU09OX0ZJTEVfTkFNRSIsImpzb25GaWxlIiwic2V0dGluZ3MiLCJUaSIsIkZpbGVzeXN0ZW0iLCJnZXRGaWxlIiwicmVzb3VyY2VzRGlyZWN0b3J5IiwiZXhpc3RzIiwiSlNPTiIsInBhcnNlIiwicmVhZCIsInRleHQiLCJBcnJheSIsImlzQXJyYXkiLCJzY3JpcHRzIiwiZXJyb3IiLCJBUEkiLCJtZXNzYWdlIiwiZmV0Y2hTY3JpcHRzRnJvbVJlc291cmNlc0RpcmVjdG9yeSIsInJlc291cmNlRGlyZWN0b3J5IiwicmVzb3VyY2VEaXJlY3RvcnlQYXRoIiwibmF0aXZlUGF0aCIsImJvb3RzdHJhcFNjcmlwdHMiLCJsb2FkRnJvbSIsImZpbGUiLCJpbmRleCIsImZpbGVOYW1lQXJyYXkiLCJib290c3RyYXBQYXRoIiwiaXNEaXJlY3RvcnkiLCJnZXREaXJlY3RvcnlMaXN0aW5nIiwibGVuZ3RoIiwibmFtZSIsInNlYXJjaCIsInN1YnN0ciIsInB1c2giLCJleHBvcnRzIiwibG9hZEFzeW5jIiwiZmluaXNoZWQiLCJzb3J0IiwibG9hZEJvb3RzdHJhcFNjcmlwdHMiLCJib290c3RyYXBJbmRleCIsImRvTG9hZCIsImZpbGVOYW1lIiwiYm9vdHN0cmFwIiwicmVxdWlyZSIsImV4ZWN1dGUiLCJvbkJvb3RzdHJhcEV4ZWN1dGlvbkZpbmlzaGVkIiwic2V0VGltZW91dCJdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7QUFnQkE7QUFDQTs7Ozs7Ozs7OztBQVVBLFNBQVNBLG9CQUFULEdBQWdDO0FBQzlCLE1BQUlDLGNBQWMsR0FBRyxnQkFBckI7QUFDSUMsRUFBQUEsUUFESjtBQUVJQyxFQUFBQSxRQUZKOztBQUlBLE1BQUk7QUFDRkQsSUFBQUEsUUFBUSxHQUFHRSxFQUFFLENBQUNDLFVBQUgsQ0FBY0MsT0FBZCxDQUFzQkYsRUFBRSxDQUFDQyxVQUFILENBQWNFLGtCQUFwQyxFQUF3RCxpQkFBaUJOLGNBQXpFLENBQVg7O0FBRUEsUUFBSUMsUUFBUSxDQUFDTSxNQUFULEVBQUosRUFBdUI7QUFDckJMLE1BQUFBLFFBQVEsR0FBR00sSUFBSSxDQUFDQyxLQUFMLENBQVdSLFFBQVEsQ0FBQ1MsSUFBVCxHQUFnQkMsSUFBM0IsQ0FBWDs7QUFFQSxVQUFJQyxLQUFLLENBQUNDLE9BQU4sQ0FBY1gsUUFBUSxDQUFDWSxPQUF2QixDQUFKLEVBQXFDO0FBQ25DLGVBQU9aLFFBQVEsQ0FBQ1ksT0FBaEI7QUFDRCxPQUZELE1BRU87QUFDTCxlQUFPLEVBQVA7QUFDRDtBQUNGO0FBQ0YsR0FaRCxDQVlFLE9BQU9DLEtBQVAsRUFBYztBQUNkWixJQUFBQSxFQUFFLENBQUNhLEdBQUgsQ0FBT0QsS0FBUCxDQUFhLHFCQUFxQmYsY0FBckIsR0FBc0MsYUFBdEMsR0FBc0RlLEtBQUssQ0FBQ0UsT0FBekU7QUFDRDs7QUFFRCxTQUFPLElBQVA7QUFDRDtBQUNEOzs7Ozs7OztBQVFBLFNBQVNDLGtDQUFULEdBQThDO0FBQzVDLE1BQUlDLGlCQUFpQixHQUFHaEIsRUFBRSxDQUFDQyxVQUFILENBQWNDLE9BQWQsQ0FBc0JGLEVBQUUsQ0FBQ0MsVUFBSCxDQUFjRSxrQkFBcEMsQ0FBeEI7QUFDSWMsRUFBQUEscUJBQXFCLEdBQUdELGlCQUFpQixDQUFDRSxVQUQ5QztBQUVJQyxFQUFBQSxnQkFBZ0IsR0FBRyxFQUZ2Qjs7QUFJQSxXQUFTQyxRQUFULENBQWtCQyxJQUFsQixFQUF3QjtBQUN0QixRQUFJQyxLQUFKLEVBQVdDLGFBQVgsRUFBMEJDLGFBQTFCOztBQUVBLFFBQUlILElBQUosRUFBVTtBQUNSLFVBQUlBLElBQUksQ0FBQ0ksV0FBTCxFQUFKLEVBQXdCO0FBQ3RCO0FBQ0FGLFFBQUFBLGFBQWEsR0FBR0YsSUFBSSxDQUFDSyxtQkFBTCxFQUFoQjs7QUFFQSxZQUFJSCxhQUFKLEVBQW1CO0FBQ2pCLGVBQUtELEtBQUssR0FBRyxDQUFiLEVBQWdCQSxLQUFLLEdBQUdDLGFBQWEsQ0FBQ0ksTUFBdEMsRUFBOENMLEtBQUssRUFBbkQsRUFBdUQ7QUFDckRGLFlBQUFBLFFBQVEsQ0FBQ3BCLEVBQUUsQ0FBQ0MsVUFBSCxDQUFjQyxPQUFkLENBQXNCbUIsSUFBSSxDQUFDSCxVQUEzQixFQUF1Q0ssYUFBYSxDQUFDRCxLQUFELENBQXBELENBQUQsQ0FBUjtBQUNEO0FBQ0Y7QUFDRixPQVRELE1BU08sSUFBSUQsSUFBSSxDQUFDTyxJQUFMLENBQVVDLE1BQVYsQ0FBaUIsZ0JBQWpCLEtBQXNDLENBQTFDLEVBQTZDO0FBQ2xEO0FBQ0E7QUFDQUwsUUFBQUEsYUFBYSxHQUFHSCxJQUFJLENBQUNILFVBQXJCO0FBQ0FNLFFBQUFBLGFBQWEsR0FBR0EsYUFBYSxDQUFDTSxNQUFkLENBQXFCYixxQkFBcUIsQ0FBQ1UsTUFBM0MsRUFBbURILGFBQWEsQ0FBQ0csTUFBZCxHQUF1QlYscUJBQXFCLENBQUNVLE1BQTdDLEdBQXNELE1BQU1BLE1BQS9HLENBQWhCO0FBQ0FSLFFBQUFBLGdCQUFnQixDQUFDWSxJQUFqQixDQUFzQlAsYUFBdEI7QUFDRDtBQUNGO0FBQ0Y7O0FBRURKLEVBQUFBLFFBQVEsQ0FBQ0osaUJBQUQsQ0FBUjtBQUNBLFNBQU9HLGdCQUFQO0FBQ0Q7QUFDRDs7Ozs7O0FBTUFhLE9BQU8sQ0FBQ0MsU0FBUixHQUFvQixVQUFVQyxRQUFWLEVBQW9CO0FBQ3RDO0FBQ0E7QUFDQTtBQUNBLE1BQUlmLGdCQUFnQixHQUFHdkIsb0JBQW9CLEVBQTNDOztBQUVBLE1BQUksQ0FBQ3VCLGdCQUFMLEVBQXVCO0FBQ3JCQSxJQUFBQSxnQkFBZ0IsR0FBR0osa0NBQWtDLEVBQXJEO0FBQ0QsR0FScUMsQ0FRcEM7OztBQUdGLE1BQUksQ0FBQ0ksZ0JBQUQsSUFBcUJBLGdCQUFnQixDQUFDUSxNQUFqQixJQUEyQixDQUFwRCxFQUF1RDtBQUNyRE8sSUFBQUEsUUFBUTtBQUNSO0FBQ0QsR0FkcUMsQ0FjcEM7OztBQUdGZixFQUFBQSxnQkFBZ0IsQ0FBQ2dCLElBQWpCLEdBakJzQyxDQWlCYjs7QUFFekIsV0FBU0Msb0JBQVQsQ0FBOEJGLFFBQTlCLEVBQXdDO0FBQ3RDLFFBQUlHLGNBQWMsR0FBRyxDQUFyQjs7QUFFQSxhQUFTQyxNQUFULEdBQWtCO0FBQ2hCO0FBQ0EsVUFBSUMsUUFBSixFQUFjQyxTQUFkOztBQUVBLGFBQU9ILGNBQWMsR0FBR2xCLGdCQUFnQixDQUFDUSxNQUF6QyxFQUFpRDtBQUMvQztBQUNBWSxRQUFBQSxRQUFRLEdBQUdwQixnQkFBZ0IsQ0FBQ2tCLGNBQUQsQ0FBM0I7QUFDQUcsUUFBQUEsU0FBUyxHQUFHQyxPQUFPLENBQUNGLFFBQUQsQ0FBbkIsQ0FIK0MsQ0FHaEI7QUFDL0I7QUFDQTs7QUFFQSxZQUFJQyxTQUFTLENBQUNFLE9BQWQsRUFBdUI7QUFDckJGLFVBQUFBLFNBQVMsQ0FBQ0UsT0FBVixDQUFrQkMsNEJBQWxCO0FBQ0E7QUFDRCxTQVY4QyxDQVU3Qzs7O0FBR0ZOLFFBQUFBLGNBQWM7QUFDZixPQWxCZSxDQWtCZDs7O0FBR0ZILE1BQUFBLFFBQVE7QUFDVDs7QUFFRCxhQUFTUyw0QkFBVCxHQUF3QztBQUN0QztBQUNBO0FBQ0FOLE1BQUFBLGNBQWM7QUFDZE8sTUFBQUEsVUFBVSxDQUFDLFlBQVk7QUFDckJOLFFBQUFBLE1BQU07QUFDUCxPQUZTLEVBRVAsQ0FGTyxDQUFWO0FBR0Q7O0FBRURBLElBQUFBLE1BQU07QUFDUDs7QUFFREYsRUFBQUEsb0JBQW9CLENBQUMsWUFBWTtBQUMvQjtBQUNBO0FBQ0FGLElBQUFBLFFBQVE7QUFDVCxHQUptQixDQUFwQjtBQUtELENBL0REIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBBcHBjZWxlcmF0b3IgVGl0YW5pdW0gTW9iaWxlXG4gKiBDb3B5cmlnaHQgKGMpIDIwMTggYnkgQXh3YXksIEluYy4gQWxsIFJpZ2h0cyBSZXNlcnZlZC5cbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSB0ZXJtcyBvZiB0aGUgQXBhY2hlIFB1YmxpYyBMaWNlbnNlXG4gKiBQbGVhc2Ugc2VlIHRoZSBMSUNFTlNFIGluY2x1ZGVkIHdpdGggdGhpcyBkaXN0cmlidXRpb24gZm9yIGRldGFpbHMuXG4gKlxuICogRGVzY3JpcHRpb246XG4gKiBUaGlzIHNjcmlwdCBsb2FkcyBhbGwgSmF2YVNjcmlwdCBmaWxlcyBlbmRpbmcgd2l0aCB0aGUgbmFtZSBcIiouYm9vdHN0cmFwLmpzXCIgYW5kIHRoZW4gZXhlY3V0ZXMgdGhlbS5cbiAqIFRoZSBtYWluIGludGVudGlvbiBvZiB0aGlzIGZlYXR1cmUgaXMgdG8gYWxsb3cgSmF2YVNjcmlwdCBmaWxlcyB0byBraWNrLW9mZiBmdW5jdGlvbmFsaXR5IG9yXG4gKiBkaXNwbGF5IFVJIHRvIHRoZSBlbmQtdXNlciBiZWZvcmUgdGhlIFwiYXBwLmpzXCIgZ2V0cyBsb2FkZWQuIFRoaXMgZmVhdHVyZSBpcyB0aGUgQ29tbW9uSlNcbiAqIGVxdWl2YWxlbnQgdG8gVGl0YW5pdW0ncyBBbmRyb2lkIG1vZHVsZSBvbkFwcENyZWF0ZSgpIG9yIGlPUyBtb2R1bGUgbG9hZCgpIGZlYXR1cmVzLlxuICpcbiAqIFVzZS1DYXNlczpcbiAqIC0gQXV0b21hdGljYWxseSBraWNrLW9mZiBhbmFseXRpY3MgZnVuY3Rpb25hbGl0eSBvbiBhcHAgc3RhcnR1cC5cbiAqIC0gRW5zdXJlIFwiR29vZ2xlIFBsYXkgU2VydmljZXNcIiBpcyBpbnN0YWxsZWQvdXBkYXRlZCBvbiBhcHAgc3RhcnR1cCBvbiBBbmRyb2lkLlxuICovXG4ndXNlIHN0cmljdCc7XG4vKipcbiAqIEF0dGVtcHRzIHRvIGxvYWQgYWxsIGJvb3RzdHJhcHMgZnJvbSBhIFwiYm9vdHN0cmFwLmpzb25cIiBmaWxlIGNyZWF0ZWQgYnkgdGhlIGFwcCBidWlsZCBzeXN0ZW0uXG4gKiBUaGlzIGlzIGFuIG9wdGlvbmFsIGZlYXR1cmUgYW5kIGlzIHRoZSBmYXN0ZXN0IG1ldGhvZCBvZiBhY3F1aXJpbmcgYm9vc3RyYXBzIGNvbmZpZ3VyZWQgZm9yIHRoZSBhcHAuXG4gKiBUaGlzIEpTT04gZmlsZSwgaWYgcHJvdmlkZWQsIG11c3QgYmUgaW4gdGhlIHNhbWUgZGlyZWN0b3J5IGFzIHRoaXMgc2NyaXB0LlxuICogQHJldHVybnMge0FycmF5LjxzdHJpbmc+fVxuICogUmV0dXJucyBhbiBhcnJheSBvZiByZXF1aXJlKCkgY29tcGF0aWJsZSBzdHJpbmdzIGlmIGJvb3RzdHJhcHMgd2VyZSBzdWNjZXNzZnVsbHkgbG9hZGVkIGZyb20gSlNPTi5cbiAqIFJldHVybnMgYW4gZW1wdHkgYXJyYXkgaWYgSlNPTiBmaWxlIHdhcyBmb3VuZCwgYnV0IG5vIGJvb3RzdHJhcHMgd2VyZSBjb25maWd1cmVkIGZvciB0aGUgYXBwLlxuICogUmV0dXJucyBudWxsIGlmIEpTT04gZmlsZSB3YXMgbm90IGZvdW5kLlxuICovXG5cbmZ1bmN0aW9uIGZldGNoU2NyaXB0c0Zyb21Kc29uKCkge1xuICB2YXIgSlNPTl9GSUxFX05BTUUgPSAnYm9vdHN0cmFwLmpzb24nLFxuICAgICAganNvbkZpbGUsXG4gICAgICBzZXR0aW5ncztcblxuICB0cnkge1xuICAgIGpzb25GaWxlID0gVGkuRmlsZXN5c3RlbS5nZXRGaWxlKFRpLkZpbGVzeXN0ZW0ucmVzb3VyY2VzRGlyZWN0b3J5LCAndGkuaW50ZXJuYWwvJyArIEpTT05fRklMRV9OQU1FKTtcblxuICAgIGlmIChqc29uRmlsZS5leGlzdHMoKSkge1xuICAgICAgc2V0dGluZ3MgPSBKU09OLnBhcnNlKGpzb25GaWxlLnJlYWQoKS50ZXh0KTtcblxuICAgICAgaWYgKEFycmF5LmlzQXJyYXkoc2V0dGluZ3Muc2NyaXB0cykpIHtcbiAgICAgICAgcmV0dXJuIHNldHRpbmdzLnNjcmlwdHM7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICByZXR1cm4gW107XG4gICAgICB9XG4gICAgfVxuICB9IGNhdGNoIChlcnJvcikge1xuICAgIFRpLkFQSS5lcnJvcignRmFpbGVkIHRvIHJlYWQgXCInICsgSlNPTl9GSUxFX05BTUUgKyAnXCIuIFJlYXNvbjogJyArIGVycm9yLm1lc3NhZ2UpO1xuICB9XG5cbiAgcmV0dXJuIG51bGw7XG59XG4vKipcbiAqIFJlY3Vyc2l2ZWx5IHNlYXJjaGVzIHRoZSBcIlJlc291cmNlc1wiIGRpcmVjdG9yeSBmb3IgYWxsIFwiKi5ib290c3RyYXAuanNcIiBmaWxlcy5cbiAqIEByZXR1cm5zIHtBcnJheS48c3RyaW5nPn1cbiAqIFJldHVybnMgYW4gYXJyYXkgb2YgcmVxdWlyZSgpIGNvbXBhdGlibGUgc3RyaW5ncyBmb3IgZWFjaCBib290c3RyYXAgZm91bmQgaW4gdGhlIHNlYXJjaC5cbiAqIFJldHVybnMgYW4gZW1wdHkgYXJyYXkgaWYgbm8gYm9vdHN0cmFwIGZpbGVzIHdlcmUgZm91bmQuXG4gKi9cblxuXG5mdW5jdGlvbiBmZXRjaFNjcmlwdHNGcm9tUmVzb3VyY2VzRGlyZWN0b3J5KCkge1xuICB2YXIgcmVzb3VyY2VEaXJlY3RvcnkgPSBUaS5GaWxlc3lzdGVtLmdldEZpbGUoVGkuRmlsZXN5c3RlbS5yZXNvdXJjZXNEaXJlY3RvcnkpLFxuICAgICAgcmVzb3VyY2VEaXJlY3RvcnlQYXRoID0gcmVzb3VyY2VEaXJlY3RvcnkubmF0aXZlUGF0aCxcbiAgICAgIGJvb3RzdHJhcFNjcmlwdHMgPSBbXTtcblxuICBmdW5jdGlvbiBsb2FkRnJvbShmaWxlKSB7XG4gICAgdmFyIGluZGV4LCBmaWxlTmFtZUFycmF5LCBib290c3RyYXBQYXRoO1xuXG4gICAgaWYgKGZpbGUpIHtcbiAgICAgIGlmIChmaWxlLmlzRGlyZWN0b3J5KCkpIHtcbiAgICAgICAgLy8gVGhpcyBpcyBhIGRpcmVjdG9yeS4gUmVjdXJzaXZlbHkgbG9vayBmb3IgYm9vdHN0cmFwIGZpbGVzIHVuZGVyIGl0LlxuICAgICAgICBmaWxlTmFtZUFycmF5ID0gZmlsZS5nZXREaXJlY3RvcnlMaXN0aW5nKCk7XG5cbiAgICAgICAgaWYgKGZpbGVOYW1lQXJyYXkpIHtcbiAgICAgICAgICBmb3IgKGluZGV4ID0gMDsgaW5kZXggPCBmaWxlTmFtZUFycmF5Lmxlbmd0aDsgaW5kZXgrKykge1xuICAgICAgICAgICAgbG9hZEZyb20oVGkuRmlsZXN5c3RlbS5nZXRGaWxlKGZpbGUubmF0aXZlUGF0aCwgZmlsZU5hbWVBcnJheVtpbmRleF0pKTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSBpZiAoZmlsZS5uYW1lLnNlYXJjaCgvLmJvb3RzdHJhcC5qcyQvKSA+PSAwKSB7XG4gICAgICAgIC8vIFRoaXMgaXMgYSBib290c3RyYXAgZmlsZS5cbiAgICAgICAgLy8gQ29udmVydCBpdHMgcGF0aCB0byBzb21ldGhpbmcgbG9hZGFibGUgdmlhIHJlcXVpcmUoKSBhbmQgYWRkIGl0IHRvIHRoZSBhcnJheS5cbiAgICAgICAgYm9vdHN0cmFwUGF0aCA9IGZpbGUubmF0aXZlUGF0aDtcbiAgICAgICAgYm9vdHN0cmFwUGF0aCA9IGJvb3RzdHJhcFBhdGguc3Vic3RyKHJlc291cmNlRGlyZWN0b3J5UGF0aC5sZW5ndGgsIGJvb3RzdHJhcFBhdGgubGVuZ3RoIC0gcmVzb3VyY2VEaXJlY3RvcnlQYXRoLmxlbmd0aCAtICcuanMnLmxlbmd0aCk7XG4gICAgICAgIGJvb3RzdHJhcFNjcmlwdHMucHVzaChib290c3RyYXBQYXRoKTtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBsb2FkRnJvbShyZXNvdXJjZURpcmVjdG9yeSk7XG4gIHJldHVybiBib290c3RyYXBTY3JpcHRzO1xufVxuLyoqXG4gKiBOb24tYmxvY2tpbmcgZnVuY3Rpb24gd2hpY2ggbG9hZHMgYW5kIGV4ZWN1dGVzIGFsbCBib290c3RyYXAgc2NyaXB0cyBjb25maWd1cmVkIGZvciB0aGUgYXBwLlxuICogQHBhcmFtIHtmdW5jdGlvbn0gZmluaXNoZWQgQ2FsbGJhY2sgdG8gYmUgaW52b2tlZCBvbmNlIGFsbCBib290c3RyYXBzIGhhdmUgZmluaXNoZWQgZXhlY3V0aW5nLiBDYW5ub3QgYmUgbnVsbC5cbiAqL1xuXG5cbmV4cG9ydHMubG9hZEFzeW5jID0gZnVuY3Rpb24gKGZpbmlzaGVkKSB7XG4gIC8vIEFjcXVpcmUgYW4gYXJyYXkgb2YgYWxsIGJvb3RzdHJhcCBzY3JpcHRzIGluY2x1ZGVkIHdpdGggdGhlIGFwcC5cbiAgLy8gLSBGb3IgYmVzdCBwZXJmb3JtYW5jZSwgYXR0ZW1wdCB0byBmZXRjaCBzY3JpcHRzIHZpYSBhbiBvcHRpb25hbCBKU09OIGZpbGUgY3JlYXRlIGJ5IHRoZSBidWlsZCBzeXN0ZW0uXG4gIC8vIC0gSWYgSlNPTiBmaWxlIG5vdCBmb3VuZCAod2lsbCByZXR1cm4gbnVsbCksIHRoZW4gc2VhcmNoIFwiUmVzb3VyY2VzXCIgZGlyZWN0b3J5IGZvciBib290c3RyYXAgZmlsZXMuXG4gIHZhciBib290c3RyYXBTY3JpcHRzID0gZmV0Y2hTY3JpcHRzRnJvbUpzb24oKTtcblxuICBpZiAoIWJvb3RzdHJhcFNjcmlwdHMpIHtcbiAgICBib290c3RyYXBTY3JpcHRzID0gZmV0Y2hTY3JpcHRzRnJvbVJlc291cmNlc0RpcmVjdG9yeSgpO1xuICB9IC8vIERvIG5vdCBjb250aW51ZSBpZiBubyBib290c3RyYXBzIHdlcmUgZm91bmQuXG5cblxuICBpZiAoIWJvb3RzdHJhcFNjcmlwdHMgfHwgYm9vdHN0cmFwU2NyaXB0cy5sZW5ndGggPD0gMCkge1xuICAgIGZpbmlzaGVkKCk7XG4gICAgcmV0dXJuO1xuICB9IC8vIFNvcnQgdGhlIGJvb3RzdHJhcHMgc28gdGhhdCB0aGV5J2xsIGJlIGxvYWRlZCBpbiBhIGNvbnNpc3RlbnQgb3JkZXIgYmV0d2VlbiBwbGF0Zm9ybXMuXG5cblxuICBib290c3RyYXBTY3JpcHRzLnNvcnQoKTsgLy8gTG9hZHMgYWxsIGJvb3RzdHJhcCBzY3JpcHRzIGZvdW5kLlxuXG4gIGZ1bmN0aW9uIGxvYWRCb290c3RyYXBTY3JpcHRzKGZpbmlzaGVkKSB7XG4gICAgdmFyIGJvb3RzdHJhcEluZGV4ID0gMDtcblxuICAgIGZ1bmN0aW9uIGRvTG9hZCgpIHtcbiAgICAgIC8vIEF0dGVtcHQgdG8gbG9hZCBhbGwgYm9vdHN0cmFwIHNjcmlwdHMuXG4gICAgICB2YXIgZmlsZU5hbWUsIGJvb3RzdHJhcDtcblxuICAgICAgd2hpbGUgKGJvb3RzdHJhcEluZGV4IDwgYm9vdHN0cmFwU2NyaXB0cy5sZW5ndGgpIHtcbiAgICAgICAgLy8gTG9hZCB0aGUgbmV4dCBib290c3RyYXAuXG4gICAgICAgIGZpbGVOYW1lID0gYm9vdHN0cmFwU2NyaXB0c1tib290c3RyYXBJbmRleF07XG4gICAgICAgIGJvb3RzdHJhcCA9IHJlcXVpcmUoZmlsZU5hbWUpOyAvLyBJbnZva2UgdGhlIGJvb3RzdHJhcCdzIGV4ZWN1dGUoKSBtZXRob2QgaWYgaXQgaGFzIG9uZS4gKFRoaXMgaXMgb3B0aW9uYWwuKVxuICAgICAgICAvLyBXZSBtdXN0IHdhaXQgZm9yIHRoZSBnaXZlbiBjYWxsYmFjayB0byBiZSBpbnZva2VkIGJlZm9yZSBsb2FkaW5nIHRoZSBuZXh0IHNjcmlwdC5cbiAgICAgICAgLy8gTm90ZTogVGhpcyBpcyBleHBlY3RlZCB0byBiZSB1c2VkIHRvIGRpc3BsYXkgVUkgdG8gdGhlIGVuZC11c2VyLlxuXG4gICAgICAgIGlmIChib290c3RyYXAuZXhlY3V0ZSkge1xuICAgICAgICAgIGJvb3RzdHJhcC5leGVjdXRlKG9uQm9vdHN0cmFwRXhlY3V0aW9uRmluaXNoZWQpO1xuICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfSAvLyBXZSdyZSBkb25lIHdpdGggdGhlIGN1cnJlbnQgYm9vdHN0cmFwLiBUaW1lIHRvIGxvYWQgdGhlIG5leHQgb25lLlxuXG5cbiAgICAgICAgYm9vdHN0cmFwSW5kZXgrKztcbiAgICAgIH0gLy8gSW52b2tlIGdpdmVuIGNhbGxiYWNrIHRvIGluZm9ybSBjYWxsZXIgdGhhdCBhbGwgbG9hZGluZyBpcyBkb25lLlxuXG5cbiAgICAgIGZpbmlzaGVkKCk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gb25Cb290c3RyYXBFeGVjdXRpb25GaW5pc2hlZCgpIHtcbiAgICAgIC8vIExhc3QgYm9vdHN0cmFwIGhhcyBmaW5pc2hlZCBleGVjdXRpb24uIFRpbWUgdG8gbG9hZCB0aGUgbmV4dCBvbmUuXG4gICAgICAvLyBOb3RlOiBBZGQgYSB0aW55IGRlbGF5IHNvIHdoYXRldmVyIFVJIHRoZSBsYXN0IGJvb3RzdHJhcCBsb2FkZWQgaGFzIHRpbWUgdG8gY2xvc2UuXG4gICAgICBib290c3RyYXBJbmRleCsrO1xuICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XG4gICAgICAgIGRvTG9hZCgpO1xuICAgICAgfSwgMSk7XG4gICAgfVxuXG4gICAgZG9Mb2FkKCk7XG4gIH1cblxuICBsb2FkQm9vdHN0cmFwU2NyaXB0cyhmdW5jdGlvbiAoKSB7XG4gICAgLy8gV2UndmUgZmluaXNoZWQgbG9hZGluZy9leGVjdXRpbmcgYWxsIGJvb3RzdHJhcCBzY3JpcHRzLlxuICAgIC8vIEluZm9ybSBjYWxsZXIgYnkgaW52b2tpbmcgdGhlIGNhbGxiYWNrIGdpdmVuIHRvIGxvYWRBc3luYygpLlxuICAgIGZpbmlzaGVkKCk7XG4gIH0pO1xufTsiXX0=