'use strict';
/**
               * This function 'standardizes' the reported architectures to the equivalents reported by Node.js
               * node values: 'arm', 'arm64', 'ia32', 'mips', 'mipsel', 'ppc', 'ppc64', 's390', 's390x', 'x32', and 'x64'.
               * iOS values: "arm64", "armv7", "x86_64", "i386", "Unknown"
               * Android values: "armeabi", "armeabi-v7a", "arm64-v8a", "x86", "x86_64", "mips", "mips64", "unknown"
               * Windows values: "x64", "ia64", "ARM", "x86", "unknown"
               * @param {string} original original architecture reported by Ti.Platform
               * @returns {string}
               */

function standardizeArch(original) {
  switch (original) {
    // coerce 'armv7', 'armeabi', 'armeabi-v7a', 'ARM' -> 'arm'
    // 'armeabi' is a dead ABI for Android, removed in NDK r17
    case 'armv7':
    case 'armeabi':
    case 'armeabi-v7a':
    case 'ARM':
      return 'arm';
    // coerce 'arm64-v8a' -> 'arm64'

    case 'arm64-v8a':
      return 'arm64';
    // coerce 'i386', 'x86' -> 'ia32'

    case 'i386':
    case 'x86':
      return 'ia32';
    // coerce 'x86_64', 'ia64', 'x64' -> 'x64'

    case 'x86_64':
    case 'ia64':
      return 'x64';
    // coerce 'mips64' -> 'mips' // 'mips' and 'mips64' are dead ABIs for Android, removed in NDK r17

    case 'mips64':
      return 'mips';
    // coerce 'Unknown' -> 'unknown'

    case 'Unknown':
      return 'unknown';

    default:
      return original;}

}

var process = {
  arch: standardizeArch(Ti.Platform.architecture),
  cwd: function cwd() {
    return __dirname;
  },
  // FIXME: Should we try and adopt 'windowsphone'/'windowsstore' to 'win32'?
  // FIXME: Should we try and adopt 'ipad'/'iphone' to 'darwin'? or 'ios'?
  platform: Ti.Platform.osname };

global.process = process;
module.exports = process;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb2Nlc3MuanMiXSwibmFtZXMiOlsic3RhbmRhcmRpemVBcmNoIiwib3JpZ2luYWwiLCJwcm9jZXNzIiwiYXJjaCIsIlRpIiwiUGxhdGZvcm0iLCJhcmNoaXRlY3R1cmUiLCJjd2QiLCJfX2Rpcm5hbWUiLCJwbGF0Zm9ybSIsIm9zbmFtZSIsImdsb2JhbCIsIm1vZHVsZSIsImV4cG9ydHMiXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7Ozs7Ozs7Ozs7QUFVQSxTQUFTQSxlQUFULENBQXlCQyxRQUF6QixFQUFtQztBQUNqQyxVQUFRQSxRQUFSO0FBQ0U7QUFDQTtBQUNBLFNBQUssT0FBTDtBQUNBLFNBQUssU0FBTDtBQUNBLFNBQUssYUFBTDtBQUNBLFNBQUssS0FBTDtBQUNFLGFBQU8sS0FBUDtBQUNGOztBQUVBLFNBQUssV0FBTDtBQUNFLGFBQU8sT0FBUDtBQUNGOztBQUVBLFNBQUssTUFBTDtBQUNBLFNBQUssS0FBTDtBQUNFLGFBQU8sTUFBUDtBQUNGOztBQUVBLFNBQUssUUFBTDtBQUNBLFNBQUssTUFBTDtBQUNFLGFBQU8sS0FBUDtBQUNGOztBQUVBLFNBQUssUUFBTDtBQUNFLGFBQU8sTUFBUDtBQUNGOztBQUVBLFNBQUssU0FBTDtBQUNFLGFBQU8sU0FBUDs7QUFFRjtBQUNFLGFBQU9BLFFBQVAsQ0FoQ0o7O0FBa0NEOztBQUVELElBQUlDLE9BQU8sR0FBRztBQUNaQyxFQUFBQSxJQUFJLEVBQUVILGVBQWUsQ0FBQ0ksRUFBRSxDQUFDQyxRQUFILENBQVlDLFlBQWIsQ0FEVDtBQUVaQyxFQUFBQSxHQUFHLEVBQUUsU0FBU0EsR0FBVCxHQUFlO0FBQ2xCLFdBQU9DLFNBQVA7QUFDRCxHQUpXO0FBS1o7QUFDQTtBQUNBQyxFQUFBQSxRQUFRLEVBQUVMLEVBQUUsQ0FBQ0MsUUFBSCxDQUFZSyxNQVBWLEVBQWQ7O0FBU0FDLE1BQU0sQ0FBQ1QsT0FBUCxHQUFpQkEsT0FBakI7QUFDQVUsTUFBTSxDQUFDQyxPQUFQLEdBQWlCWCxPQUFqQiIsInNvdXJjZXNDb250ZW50IjpbIid1c2Ugc3RyaWN0Jztcbi8qKlxuICogVGhpcyBmdW5jdGlvbiAnc3RhbmRhcmRpemVzJyB0aGUgcmVwb3J0ZWQgYXJjaGl0ZWN0dXJlcyB0byB0aGUgZXF1aXZhbGVudHMgcmVwb3J0ZWQgYnkgTm9kZS5qc1xuICogbm9kZSB2YWx1ZXM6ICdhcm0nLCAnYXJtNjQnLCAnaWEzMicsICdtaXBzJywgJ21pcHNlbCcsICdwcGMnLCAncHBjNjQnLCAnczM5MCcsICdzMzkweCcsICd4MzInLCBhbmQgJ3g2NCcuXG4gKiBpT1MgdmFsdWVzOiBcImFybTY0XCIsIFwiYXJtdjdcIiwgXCJ4ODZfNjRcIiwgXCJpMzg2XCIsIFwiVW5rbm93blwiXG4gKiBBbmRyb2lkIHZhbHVlczogXCJhcm1lYWJpXCIsIFwiYXJtZWFiaS12N2FcIiwgXCJhcm02NC12OGFcIiwgXCJ4ODZcIiwgXCJ4ODZfNjRcIiwgXCJtaXBzXCIsIFwibWlwczY0XCIsIFwidW5rbm93blwiXG4gKiBXaW5kb3dzIHZhbHVlczogXCJ4NjRcIiwgXCJpYTY0XCIsIFwiQVJNXCIsIFwieDg2XCIsIFwidW5rbm93blwiXG4gKiBAcGFyYW0ge3N0cmluZ30gb3JpZ2luYWwgb3JpZ2luYWwgYXJjaGl0ZWN0dXJlIHJlcG9ydGVkIGJ5IFRpLlBsYXRmb3JtXG4gKiBAcmV0dXJucyB7c3RyaW5nfVxuICovXG5cbmZ1bmN0aW9uIHN0YW5kYXJkaXplQXJjaChvcmlnaW5hbCkge1xuICBzd2l0Y2ggKG9yaWdpbmFsKSB7XG4gICAgLy8gY29lcmNlICdhcm12NycsICdhcm1lYWJpJywgJ2FybWVhYmktdjdhJywgJ0FSTScgLT4gJ2FybSdcbiAgICAvLyAnYXJtZWFiaScgaXMgYSBkZWFkIEFCSSBmb3IgQW5kcm9pZCwgcmVtb3ZlZCBpbiBOREsgcjE3XG4gICAgY2FzZSAnYXJtdjcnOlxuICAgIGNhc2UgJ2FybWVhYmknOlxuICAgIGNhc2UgJ2FybWVhYmktdjdhJzpcbiAgICBjYXNlICdBUk0nOlxuICAgICAgcmV0dXJuICdhcm0nO1xuICAgIC8vIGNvZXJjZSAnYXJtNjQtdjhhJyAtPiAnYXJtNjQnXG5cbiAgICBjYXNlICdhcm02NC12OGEnOlxuICAgICAgcmV0dXJuICdhcm02NCc7XG4gICAgLy8gY29lcmNlICdpMzg2JywgJ3g4NicgLT4gJ2lhMzInXG5cbiAgICBjYXNlICdpMzg2JzpcbiAgICBjYXNlICd4ODYnOlxuICAgICAgcmV0dXJuICdpYTMyJztcbiAgICAvLyBjb2VyY2UgJ3g4Nl82NCcsICdpYTY0JywgJ3g2NCcgLT4gJ3g2NCdcblxuICAgIGNhc2UgJ3g4Nl82NCc6XG4gICAgY2FzZSAnaWE2NCc6XG4gICAgICByZXR1cm4gJ3g2NCc7XG4gICAgLy8gY29lcmNlICdtaXBzNjQnIC0+ICdtaXBzJyAvLyAnbWlwcycgYW5kICdtaXBzNjQnIGFyZSBkZWFkIEFCSXMgZm9yIEFuZHJvaWQsIHJlbW92ZWQgaW4gTkRLIHIxN1xuXG4gICAgY2FzZSAnbWlwczY0JzpcbiAgICAgIHJldHVybiAnbWlwcyc7XG4gICAgLy8gY29lcmNlICdVbmtub3duJyAtPiAndW5rbm93bidcblxuICAgIGNhc2UgJ1Vua25vd24nOlxuICAgICAgcmV0dXJuICd1bmtub3duJztcblxuICAgIGRlZmF1bHQ6XG4gICAgICByZXR1cm4gb3JpZ2luYWw7XG4gIH1cbn1cblxudmFyIHByb2Nlc3MgPSB7XG4gIGFyY2g6IHN0YW5kYXJkaXplQXJjaChUaS5QbGF0Zm9ybS5hcmNoaXRlY3R1cmUpLFxuICBjd2Q6IGZ1bmN0aW9uIGN3ZCgpIHtcbiAgICByZXR1cm4gX19kaXJuYW1lO1xuICB9LFxuICAvLyBGSVhNRTogU2hvdWxkIHdlIHRyeSBhbmQgYWRvcHQgJ3dpbmRvd3NwaG9uZScvJ3dpbmRvd3NzdG9yZScgdG8gJ3dpbjMyJz9cbiAgLy8gRklYTUU6IFNob3VsZCB3ZSB0cnkgYW5kIGFkb3B0ICdpcGFkJy8naXBob25lJyB0byAnZGFyd2luJz8gb3IgJ2lvcyc/XG4gIHBsYXRmb3JtOiBUaS5QbGF0Zm9ybS5vc25hbWVcbn07XG5nbG9iYWwucHJvY2VzcyA9IHByb2Nlc3M7XG5tb2R1bGUuZXhwb3J0cyA9IHByb2Nlc3M7Il19