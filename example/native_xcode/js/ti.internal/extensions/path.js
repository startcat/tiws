'use strict';

var isWin32 = Ti.Platform.osname === 'windowsphone' || Ti.Platform.osname === 'windowsstore';
var FORWARD_SLASH = 47; // '/'

var BACKWARD_SLASH = 92; // '\\'

/**
 * Is this [a-zA-Z]?
 * @param  {number}  charCode value from String.charCodeAt()
 * @return {Boolean}          [description]
 */

function isWindowsDeviceName(charCode) {
  return charCode >= 65 && charCode <= 90 || charCode >= 97 && charCode <= 122;
}
/**
   * [assertType description]
   * @param  {*} arg      passed in argument value
   * @param  {string} name     name of the argument
   * @param  {string} typename i.e. 'string'
   * @return {void}
   */


function assertType(arg, name, typename) {
  var type = typeof arg;

  if (type !== typename) {
    throw new TypeError(`The "${name}" argument must be of type ${typename}. Received type ${type}`);
  }
}
/**
   * [isAbsolute description]
   * @param  {boolean} isPosix whether this impl is for POSIX or not
   * @param  {string} filepath   input file path
   * @return {Boolean}          [description]
   */


function _isAbsolute(isPosix, filepath) {
  assertType(filepath, 'path', 'string');
  var length = filepath.length; // empty string special case

  if (length === 0) {
    return false;
  }

  var firstChar = filepath.charCodeAt(0);

  if (firstChar === FORWARD_SLASH) {
    return true;
  } // we already did our checks for posix


  if (isPosix) {
    return false;
  } // win32 from here on out


  if (firstChar === BACKWARD_SLASH) {
    return true;
  }

  if (length > 2 && isWindowsDeviceName(firstChar) && filepath.charAt(1) === ':') {
    var thirdChar = filepath.charAt(2);
    return thirdChar === '/' || thirdChar === '\\';
  }

  return false;
}
/**
   * [dirname description]
   * @param  {string} separator  platform-specific file separator
   * @param  {string} filepath   input file path
   * @return {string}            [description]
   */


function _dirname(separator, filepath) {
  assertType(filepath, 'path', 'string');
  var length = filepath.length;

  if (length === 0) {
    return '.';
  } // ignore trailing separator


  var fromIndex = length - 1;
  var hadTrailing = filepath.endsWith(separator);

  if (hadTrailing) {
    fromIndex--;
  }

  var foundIndex = filepath.lastIndexOf(separator, fromIndex); // no separators

  if (foundIndex === -1) {
    // handle special case of root windows paths
    if (length >= 2 && separator === '\\' && filepath.charAt(1) === ':') {
      var firstChar = filepath.charCodeAt(0);

      if (isWindowsDeviceName(firstChar)) {
        return filepath; // it's a root windows path
      }
    }

    return '.';
  } // only found root separator


  if (foundIndex === 0) {
    return separator; // if it was '/', return that
  } // Handle special case of '//something'


  if (foundIndex === 1 && separator === '/' && filepath.charAt(0) === '/') {
    return '//';
  }

  return filepath.slice(0, foundIndex);
}
/**
   * [extname description]
   * @param  {string} separator  platform-specific file separator
   * @param  {string} filepath   input file path
   * @return {string}            [description]
   */


function _extname(separator, filepath) {
  assertType(filepath, 'path', 'string');
  var index = filepath.lastIndexOf('.');

  if (index === -1 || index === 0) {
    return '';
  } // ignore trailing separator


  var endIndex = filepath.length;

  if (filepath.endsWith(separator)) {
    endIndex--;
  }

  return filepath.slice(index, endIndex);
}

function lastIndexWin32Separator(filepath, index) {
  for (var i = index; i >= 0; i--) {
    var char = filepath.charCodeAt(i);

    if (char === BACKWARD_SLASH || char === FORWARD_SLASH) {
      return i;
    }
  }

  return -1;
}
/**
   * [basename description]
   * @param  {string} separator  platform-specific file separator
   * @param  {string} filepath   input file path
   * @param  {string} [ext]      file extension to drop if it exists
   * @return {string}            [description]
   */


function _basename(separator, filepath, ext) {
  assertType(filepath, 'path', 'string');

  if (ext !== undefined) {
    assertType(ext, 'ext', 'string');
  }

  var length = filepath.length;

  if (length === 0) {
    return '';
  }

  var isPosix = separator === '/';
  var endIndex = length; // drop trailing separator (if there is one)

  var lastCharCode = filepath.charCodeAt(length - 1);

  if (lastCharCode === FORWARD_SLASH || !isPosix && lastCharCode === BACKWARD_SLASH) {
    endIndex--;
  } // Find last occurence of separator


  var lastIndex = -1;

  if (isPosix) {
    lastIndex = filepath.lastIndexOf(separator, endIndex - 1);
  } else {
    // On win32, handle *either* separator!
    lastIndex = lastIndexWin32Separator(filepath, endIndex - 1); // handle special case of root path like 'C:' or 'C:\\'

    if ((lastIndex === 2 || lastIndex === -1) && filepath.charAt(1) === ':' && isWindowsDeviceName(filepath.charCodeAt(0))) {
      return '';
    }
  } // Take from last occurrence of separator to end of string (or beginning to end if not found)


  var base = filepath.slice(lastIndex + 1, endIndex); // drop trailing extension (if specified)

  if (ext === undefined) {
    return base;
  }

  return base.endsWith(ext) ? base.slice(0, base.length - ext.length) : base;
}
/**
   * The `path.normalize()` method normalizes the given path, resolving '..' and '.' segments.
   *
   * When multiple, sequential path segment separation characters are found (e.g.
   * / on POSIX and either \ or / on Windows), they are replaced by a single
   * instance of the platform-specific path segment separator (/ on POSIX and \
   * on Windows). Trailing separators are preserved.
   *
   * If the path is a zero-length string, '.' is returned, representing the
   * current working directory.
   *
   * @param  {string} separator  platform-specific file separator
   * @param  {string} filepath  input file path
   * @return {string} [description]
   */


function _normalize(separator, filepath) {
  assertType(filepath, 'path', 'string');

  if (filepath.length === 0) {
    return '.';
  } // Windows can handle '/' or '\\' and both should be turned into separator


  var isWindows = separator === '\\';

  if (isWindows) {
    filepath = filepath.replace(/\//g, separator);
  }

  var hadLeading = filepath.startsWith(separator); // On Windows, need to handle UNC paths (\\host-name\\resource\\dir) special to retain leading double backslash

  var isUNC = hadLeading && isWindows && filepath.length > 2 && filepath.charAt(1) === '\\';
  var hadTrailing = filepath.endsWith(separator);
  var parts = filepath.split(separator);
  var result = [];
  var _iteratorNormalCompletion = true;
  var _didIteratorError = false;
  var _iteratorError = undefined;

  try {
    for (var _iterator = parts[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
      var segment = _step.value;

      if (segment.length !== 0 && segment !== '.') {
        if (segment === '..') {
          result.pop(); // FIXME: What if this goes above root? Should we throw an error?
        } else {
          result.push(segment);
        }
      }
    }
  } catch (err) {
    _didIteratorError = true;
    _iteratorError = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion && _iterator.return != null) {
        _iterator.return();
      }
    } finally {
      if (_didIteratorError) {
        throw _iteratorError;
      }
    }
  }

  var normalized = hadLeading ? separator : '';
  normalized += result.join(separator);

  if (hadTrailing) {
    normalized += separator;
  }

  if (isUNC) {
    normalized = '\\' + normalized;
  }

  return normalized;
}
/**
   * [assertSegment description]
   * @param  {*} segment [description]
   * @return {void}         [description]
   */


function assertSegment(segment) {
  if (typeof segment !== 'string') {
    throw new TypeError(`Path must be a string. Received ${segment}`);
  }
}
/**
   * The `path.join()` method joins all given path segments together using the
   * platform-specific separator as a delimiter, then normalizes the resulting path.
   * Zero-length path segments are ignored. If the joined path string is a zero-
   * length string then '.' will be returned, representing the current working directory.
   * @param  {string} separator platform-specific file separator
   * @param  {string[]} paths [description]
   * @return {string}       The joined filepath
   */


function _join(separator, paths) {
  var result = []; // naive impl: just join all the paths with separator

  var _iteratorNormalCompletion2 = true;
  var _didIteratorError2 = false;
  var _iteratorError2 = undefined;

  try {
    for (var _iterator2 = paths[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
      var segment = _step2.value;
      assertSegment(segment);

      if (segment.length !== 0) {
        result.push(segment);
      }
    }
  } catch (err) {
    _didIteratorError2 = true;
    _iteratorError2 = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion2 && _iterator2.return != null) {
        _iterator2.return();
      }
    } finally {
      if (_didIteratorError2) {
        throw _iteratorError2;
      }
    }
  }

  return _normalize(separator, result.join(separator));
}
/**
   * The `path.resolve()` method resolves a sequence of paths or path segments into an absolute path.
   *
   * @param  {string} separator platform-specific file separator
   * @param  {string[]} paths [description]
   * @return {string}       [description]
   */


function _resolve(separator, paths) {
  var resolved = '';
  var hitRoot = false;
  var isPosix = separator === '/'; // go from right to left until we hit absolute path/root

  for (var i = paths.length - 1; i >= 0; i--) {
    var segment = paths[i];
    assertSegment(segment);

    if (segment.length === 0) {
      continue; // skip empty
    }

    resolved = segment + separator + resolved; // prepend new segment

    if (_isAbsolute(isPosix, segment)) {
      // have we backed into an absolute path?
      hitRoot = true;
      break;
    }
  } // if we didn't hit root, prepend cwd


  if (!hitRoot) {
    resolved = process.cwd() + separator + resolved;
  }

  var normalized = _normalize(separator, resolved);

  if (normalized.charAt(normalized.length - 1) === separator) {
    // FIXME: Handle UNC paths on Windows as well, so we don't trim trailing separator on something like '\\\\host-name\\resource\\'
    // Don't remove trailing separator if this is root path on windows!
    if (!isPosix && normalized.length === 3 && normalized.charAt(1) === ':' && isWindowsDeviceName(normalized.charCodeAt(0))) {
      return normalized;
    } // otherwise trim trailing separator


    return normalized.slice(0, normalized.length - 1);
  }

  return normalized;
}
/**
   * The `path.relative()` method returns the relative path `from` from to `to` based
   * on the current working directory. If from and to each resolve to the same
   * path (after calling `path.resolve()` on each), a zero-length string is returned.
   *
   * If a zero-length string is passed as `from` or `to`, the current working directory
   * will be used instead of the zero-length strings.
   *
   * @param  {string} separator platform-specific file separator
   * @param  {string} from [description]
   * @param  {string} to   [description]
   * @return {string}      [description]
   */


function _relative(separator, from, to) {
  assertType(from, 'from', 'string');
  assertType(to, 'to', 'string');

  if (from === to) {
    return '';
  }

  from = _resolve(separator, [from]);
  to = _resolve(separator, [to]);

  if (from === to) {
    return '';
  } // we now have two absolute paths,
  // lets "go up" from `from` until we reach common base dir of `to`
  // const originalFrom = from;


  var upCount = 0;
  var remainingPath = '';

  while (true) {
    if (to.startsWith(from)) {
      // match! record rest...?
      remainingPath = to.slice(from.length);
      break;
    } // FIXME: Break/throw if we hit bad edge case of no common root!


    from = _dirname(separator, from);
    upCount++;
  } // remove leading separator from remainingPath if there is any


  if (remainingPath.length > 0) {
    remainingPath = remainingPath.slice(1);
  }

  return ('..' + separator).repeat(upCount) + remainingPath;
}
/**
   * The `path.parse()` method returns an object whose properties represent
   * significant elements of the path. Trailing directory separators are ignored,
   * see `path.sep`.
   *
   * The returned object will have the following properties:
   *
   * - dir <string>
   * - root <string>
   * - base <string>
   * - name <string>
   * - ext <string>
   * @param  {string} separator platform-specific file separator
   * @param  {string} filepath [description]
   * @return {object}
   */


function _parse(separator, filepath) {
  assertType(filepath, 'path', 'string');
  var result = {
    root: '',
    dir: '',
    base: '',
    ext: '',
    name: '' };

  var length = filepath.length;

  if (length === 0) {
    return result;
  } // Cheat and just call our other methods for dirname/basename/extname?


  result.base = _basename(separator, filepath);
  result.ext = _extname(separator, result.base);
  var baseLength = result.base.length;
  result.name = result.base.slice(0, baseLength - result.ext.length);
  var toSubtract = baseLength === 0 ? 0 : baseLength + 1;
  result.dir = filepath.slice(0, filepath.length - toSubtract); // drop trailing separator!

  var firstCharCode = filepath.charCodeAt(0); // both win32 and POSIX return '/' root

  if (firstCharCode === FORWARD_SLASH) {
    result.root = '/';
    return result;
  } // we're done with POSIX...


  if (separator === '/') {
    return result;
  } // for win32...


  if (firstCharCode === BACKWARD_SLASH) {
    // FIXME: Handle UNC paths like '\\\\host-name\\resource\\file_path'
    // need to retain '\\\\host-name\\resource\\' as root in that case!
    result.root = '\\';
    return result;
  } // check for C: style root


  if (length > 1 && isWindowsDeviceName(firstCharCode) && filepath.charAt(1) === ':') {
    if (length > 2) {
      // is it like C:\\?
      var thirdCharCode = filepath.charCodeAt(2);

      if (thirdCharCode === FORWARD_SLASH || thirdCharCode === BACKWARD_SLASH) {
        result.root = filepath.slice(0, 3);
        return result;
      }
    } // nope, just C:, no trailing separator


    result.root = filepath.slice(0, 2);
  }

  return result;
}
/**
   * The `path.format()` method returns a path string from an object. This is the
   * opposite of `path.parse()`.
   *
   * @param  {string} separator platform-specific file separator
   * @param  {object} pathObject object of format returned by `path.parse()`
   * @param  {string} pathObject.dir directory name
   * @param  {string} pathObject.root file root dir, ignored if `pathObject.dir` is provided
   * @param  {string} pathObject.base file basename
   * @param  {string} pathObject.name basename minus extension, ignored if `pathObject.base` exists
   * @param  {string} pathObject.ext file extension, ignored if `pathObject.base` exists
   * @return {string}
   */


function _format(separator, pathObject) {
  assertType(pathObject, 'pathObject', 'object');
  var base = pathObject.base || `${pathObject.name || ''}${pathObject.ext || ''}`; // append base to root if `dir` wasn't specified, or if
  // dir is the root

  if (!pathObject.dir || pathObject.dir === pathObject.root) {
    return `${pathObject.root || ''}${base}`;
  } // combine dir + / + base


  return `${pathObject.dir}${separator}${base}`;
}
/**
   * On Windows systems only, returns an equivalent namespace-prefixed path for
   * the given path. If path is not a string, path will be returned without modifications.
   * See https://docs.microsoft.com/en-us/windows/desktop/FileIO/naming-a-file#namespaces
   * @param  {string} filepath [description]
   * @return {string}          [description]
   */


function toNamespacedPath(filepath) {
  if (typeof filepath !== 'string') {
    return filepath;
  }

  if (filepath.length === 0) {
    return '';
  }

  var resolvedPath = _resolve('\\', [filepath]);

  var length = resolvedPath.length;

  if (length < 2) {
    // need '\\\\' or 'C:' minimum
    return filepath;
  }

  var firstCharCode = resolvedPath.charCodeAt(0); // if start with '\\\\', prefix with UNC root, drop the slashes

  if (firstCharCode === BACKWARD_SLASH && resolvedPath.charAt(1) === '\\') {
    // return as-is if it's an aready long path ('\\\\?\\' or '\\\\.\\' prefix)
    if (length >= 3) {
      var thirdChar = resolvedPath.charAt(2);

      if (thirdChar === '?' || thirdChar === '.') {
        return filepath;
      }
    }

    return '\\\\?\\UNC\\' + resolvedPath.slice(2);
  } else if (isWindowsDeviceName(firstCharCode) && resolvedPath.charAt(1) === ':') {
    return '\\\\?\\' + resolvedPath;
  }

  return filepath;
}

var Win32Path = {
  sep: '\\',
  delimiter: ';',
  basename: function basename(filepath, ext) {
    return _basename(this.sep, filepath, ext);
  },
  normalize: function normalize(filepath) {
    return _normalize(this.sep, filepath);
  },
  join: function join() {
    for (var _len = arguments.length, paths = new Array(_len), _key = 0; _key < _len; _key++) {
      paths[_key] = arguments[_key];
    }

    return _join(this.sep, paths);
  },
  extname: function extname(filepath) {
    return _extname(this.sep, filepath);
  },
  dirname: function dirname(filepath) {
    return _dirname(this.sep, filepath);
  },
  isAbsolute: function isAbsolute(filepath) {
    return _isAbsolute(false, filepath);
  },
  relative: function relative(from, to) {
    return _relative(this.sep, from, to);
  },
  resolve: function resolve() {
    for (var _len2 = arguments.length, paths = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
      paths[_key2] = arguments[_key2];
    }

    return _resolve(this.sep, paths);
  },
  parse: function parse(filepath) {
    return _parse(this.sep, filepath);
  },
  format: function format(pathObject) {
    return _format(this.sep, pathObject);
  },
  toNamespacedPath: toNamespacedPath };

var PosixPath = {
  sep: '/',
  delimiter: ':',
  basename: function basename(filepath, ext) {
    return _basename(this.sep, filepath, ext);
  },
  normalize: function normalize(filepath) {
    return _normalize(this.sep, filepath);
  },
  join: function join() {
    for (var _len3 = arguments.length, paths = new Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
      paths[_key3] = arguments[_key3];
    }

    return _join(this.sep, paths);
  },
  extname: function extname(filepath) {
    return _extname(this.sep, filepath);
  },
  dirname: function dirname(filepath) {
    return _dirname(this.sep, filepath);
  },
  isAbsolute: function isAbsolute(filepath) {
    return _isAbsolute(true, filepath);
  },
  relative: function relative(from, to) {
    return _relative(this.sep, from, to);
  },
  resolve: function resolve() {
    for (var _len4 = arguments.length, paths = new Array(_len4), _key4 = 0; _key4 < _len4; _key4++) {
      paths[_key4] = arguments[_key4];
    }

    return _resolve(this.sep, paths);
  },
  parse: function parse(filepath) {
    return _parse(this.sep, filepath);
  },
  format: function format(pathObject) {
    return _format(this.sep, pathObject);
  },
  toNamespacedPath: function toNamespacedPath(filepath) {
    return filepath; // no-op
  } };

var path = isWin32 ? Win32Path : PosixPath;
path.win32 = Win32Path;
path.posix = PosixPath;
module.exports = path;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInBhdGguanMiXSwibmFtZXMiOlsiaXNXaW4zMiIsIlRpIiwiUGxhdGZvcm0iLCJvc25hbWUiLCJGT1JXQVJEX1NMQVNIIiwiQkFDS1dBUkRfU0xBU0giLCJpc1dpbmRvd3NEZXZpY2VOYW1lIiwiY2hhckNvZGUiLCJhc3NlcnRUeXBlIiwiYXJnIiwibmFtZSIsInR5cGVuYW1lIiwidHlwZSIsIlR5cGVFcnJvciIsIl9pc0Fic29sdXRlIiwiaXNQb3NpeCIsImZpbGVwYXRoIiwibGVuZ3RoIiwiZmlyc3RDaGFyIiwiY2hhckNvZGVBdCIsImNoYXJBdCIsInRoaXJkQ2hhciIsIl9kaXJuYW1lIiwic2VwYXJhdG9yIiwiZnJvbUluZGV4IiwiaGFkVHJhaWxpbmciLCJlbmRzV2l0aCIsImZvdW5kSW5kZXgiLCJsYXN0SW5kZXhPZiIsInNsaWNlIiwiX2V4dG5hbWUiLCJpbmRleCIsImVuZEluZGV4IiwibGFzdEluZGV4V2luMzJTZXBhcmF0b3IiLCJpIiwiY2hhciIsIl9iYXNlbmFtZSIsImV4dCIsInVuZGVmaW5lZCIsImxhc3RDaGFyQ29kZSIsImxhc3RJbmRleCIsImJhc2UiLCJfbm9ybWFsaXplIiwiaXNXaW5kb3dzIiwicmVwbGFjZSIsImhhZExlYWRpbmciLCJzdGFydHNXaXRoIiwiaXNVTkMiLCJwYXJ0cyIsInNwbGl0IiwicmVzdWx0IiwiX2l0ZXJhdG9yTm9ybWFsQ29tcGxldGlvbiIsIl9kaWRJdGVyYXRvckVycm9yIiwiX2l0ZXJhdG9yRXJyb3IiLCJfaXRlcmF0b3IiLCJTeW1ib2wiLCJpdGVyYXRvciIsIl9zdGVwIiwibmV4dCIsImRvbmUiLCJzZWdtZW50IiwidmFsdWUiLCJwb3AiLCJwdXNoIiwiZXJyIiwicmV0dXJuIiwibm9ybWFsaXplZCIsImpvaW4iLCJhc3NlcnRTZWdtZW50IiwiX2pvaW4iLCJwYXRocyIsIl9pdGVyYXRvck5vcm1hbENvbXBsZXRpb24yIiwiX2RpZEl0ZXJhdG9yRXJyb3IyIiwiX2l0ZXJhdG9yRXJyb3IyIiwiX2l0ZXJhdG9yMiIsIl9zdGVwMiIsIl9yZXNvbHZlIiwicmVzb2x2ZWQiLCJoaXRSb290IiwicHJvY2VzcyIsImN3ZCIsIl9yZWxhdGl2ZSIsImZyb20iLCJ0byIsInVwQ291bnQiLCJyZW1haW5pbmdQYXRoIiwicmVwZWF0IiwiX3BhcnNlIiwicm9vdCIsImRpciIsImJhc2VMZW5ndGgiLCJ0b1N1YnRyYWN0IiwiZmlyc3RDaGFyQ29kZSIsInRoaXJkQ2hhckNvZGUiLCJfZm9ybWF0IiwicGF0aE9iamVjdCIsInRvTmFtZXNwYWNlZFBhdGgiLCJyZXNvbHZlZFBhdGgiLCJXaW4zMlBhdGgiLCJzZXAiLCJkZWxpbWl0ZXIiLCJiYXNlbmFtZSIsIm5vcm1hbGl6ZSIsIl9sZW4iLCJhcmd1bWVudHMiLCJBcnJheSIsIl9rZXkiLCJleHRuYW1lIiwiZGlybmFtZSIsImlzQWJzb2x1dGUiLCJyZWxhdGl2ZSIsInJlc29sdmUiLCJfbGVuMiIsIl9rZXkyIiwicGFyc2UiLCJmb3JtYXQiLCJQb3NpeFBhdGgiLCJfbGVuMyIsIl9rZXkzIiwiX2xlbjQiLCJfa2V5NCIsInBhdGgiLCJ3aW4zMiIsInBvc2l4IiwibW9kdWxlIiwiZXhwb3J0cyJdLCJtYXBwaW5ncyI6IkFBQUE7O0FBRUEsSUFBSUEsT0FBTyxHQUFHQyxFQUFFLENBQUNDLFFBQUgsQ0FBWUMsTUFBWixLQUF1QixjQUF2QixJQUF5Q0YsRUFBRSxDQUFDQyxRQUFILENBQVlDLE1BQVosS0FBdUIsY0FBOUU7QUFDQSxJQUFJQyxhQUFhLEdBQUcsRUFBcEIsQyxDQUF3Qjs7QUFFeEIsSUFBSUMsY0FBYyxHQUFHLEVBQXJCLEMsQ0FBeUI7O0FBRXpCOzs7Ozs7QUFNQSxTQUFTQyxtQkFBVCxDQUE2QkMsUUFBN0IsRUFBdUM7QUFDckMsU0FBT0EsUUFBUSxJQUFJLEVBQVosSUFBa0JBLFFBQVEsSUFBSSxFQUE5QixJQUFvQ0EsUUFBUSxJQUFJLEVBQVosSUFBa0JBLFFBQVEsSUFBSSxHQUF6RTtBQUNEO0FBQ0Q7Ozs7Ozs7OztBQVNBLFNBQVNDLFVBQVQsQ0FBb0JDLEdBQXBCLEVBQXlCQyxJQUF6QixFQUErQkMsUUFBL0IsRUFBeUM7QUFDdkMsTUFBSUMsSUFBSSxHQUFHLE9BQU9ILEdBQWxCOztBQUVBLE1BQUlHLElBQUksS0FBS0QsUUFBYixFQUF1QjtBQUNyQixVQUFNLElBQUlFLFNBQUosQ0FBZSxRQUFPSCxJQUFLLDhCQUE2QkMsUUFBUyxtQkFBa0JDLElBQUssRUFBeEYsQ0FBTjtBQUNEO0FBQ0Y7QUFDRDs7Ozs7Ozs7QUFRQSxTQUFTRSxXQUFULENBQXFCQyxPQUFyQixFQUE4QkMsUUFBOUIsRUFBd0M7QUFDdENSLEVBQUFBLFVBQVUsQ0FBQ1EsUUFBRCxFQUFXLE1BQVgsRUFBbUIsUUFBbkIsQ0FBVjtBQUNBLE1BQUlDLE1BQU0sR0FBR0QsUUFBUSxDQUFDQyxNQUF0QixDQUZzQyxDQUVSOztBQUU5QixNQUFJQSxNQUFNLEtBQUssQ0FBZixFQUFrQjtBQUNoQixXQUFPLEtBQVA7QUFDRDs7QUFFRCxNQUFJQyxTQUFTLEdBQUdGLFFBQVEsQ0FBQ0csVUFBVCxDQUFvQixDQUFwQixDQUFoQjs7QUFFQSxNQUFJRCxTQUFTLEtBQUtkLGFBQWxCLEVBQWlDO0FBQy9CLFdBQU8sSUFBUDtBQUNELEdBWnFDLENBWXBDOzs7QUFHRixNQUFJVyxPQUFKLEVBQWE7QUFDWCxXQUFPLEtBQVA7QUFDRCxHQWpCcUMsQ0FpQnBDOzs7QUFHRixNQUFJRyxTQUFTLEtBQUtiLGNBQWxCLEVBQWtDO0FBQ2hDLFdBQU8sSUFBUDtBQUNEOztBQUVELE1BQUlZLE1BQU0sR0FBRyxDQUFULElBQWNYLG1CQUFtQixDQUFDWSxTQUFELENBQWpDLElBQWdERixRQUFRLENBQUNJLE1BQVQsQ0FBZ0IsQ0FBaEIsTUFBdUIsR0FBM0UsRUFBZ0Y7QUFDOUUsUUFBSUMsU0FBUyxHQUFHTCxRQUFRLENBQUNJLE1BQVQsQ0FBZ0IsQ0FBaEIsQ0FBaEI7QUFDQSxXQUFPQyxTQUFTLEtBQUssR0FBZCxJQUFxQkEsU0FBUyxLQUFLLElBQTFDO0FBQ0Q7O0FBRUQsU0FBTyxLQUFQO0FBQ0Q7QUFDRDs7Ozs7Ozs7QUFRQSxTQUFTQyxRQUFULENBQWtCQyxTQUFsQixFQUE2QlAsUUFBN0IsRUFBdUM7QUFDckNSLEVBQUFBLFVBQVUsQ0FBQ1EsUUFBRCxFQUFXLE1BQVgsRUFBbUIsUUFBbkIsQ0FBVjtBQUNBLE1BQUlDLE1BQU0sR0FBR0QsUUFBUSxDQUFDQyxNQUF0Qjs7QUFFQSxNQUFJQSxNQUFNLEtBQUssQ0FBZixFQUFrQjtBQUNoQixXQUFPLEdBQVA7QUFDRCxHQU5vQyxDQU1uQzs7O0FBR0YsTUFBSU8sU0FBUyxHQUFHUCxNQUFNLEdBQUcsQ0FBekI7QUFDQSxNQUFJUSxXQUFXLEdBQUdULFFBQVEsQ0FBQ1UsUUFBVCxDQUFrQkgsU0FBbEIsQ0FBbEI7O0FBRUEsTUFBSUUsV0FBSixFQUFpQjtBQUNmRCxJQUFBQSxTQUFTO0FBQ1Y7O0FBRUQsTUFBSUcsVUFBVSxHQUFHWCxRQUFRLENBQUNZLFdBQVQsQ0FBcUJMLFNBQXJCLEVBQWdDQyxTQUFoQyxDQUFqQixDQWhCcUMsQ0FnQndCOztBQUU3RCxNQUFJRyxVQUFVLEtBQUssQ0FBQyxDQUFwQixFQUF1QjtBQUNyQjtBQUNBLFFBQUlWLE1BQU0sSUFBSSxDQUFWLElBQWVNLFNBQVMsS0FBSyxJQUE3QixJQUFxQ1AsUUFBUSxDQUFDSSxNQUFULENBQWdCLENBQWhCLE1BQXVCLEdBQWhFLEVBQXFFO0FBQ25FLFVBQUlGLFNBQVMsR0FBR0YsUUFBUSxDQUFDRyxVQUFULENBQW9CLENBQXBCLENBQWhCOztBQUVBLFVBQUliLG1CQUFtQixDQUFDWSxTQUFELENBQXZCLEVBQW9DO0FBQ2xDLGVBQU9GLFFBQVAsQ0FEa0MsQ0FDakI7QUFDbEI7QUFDRjs7QUFFRCxXQUFPLEdBQVA7QUFDRCxHQTdCb0MsQ0E2Qm5DOzs7QUFHRixNQUFJVyxVQUFVLEtBQUssQ0FBbkIsRUFBc0I7QUFDcEIsV0FBT0osU0FBUCxDQURvQixDQUNGO0FBQ25CLEdBbENvQyxDQWtDbkM7OztBQUdGLE1BQUlJLFVBQVUsS0FBSyxDQUFmLElBQW9CSixTQUFTLEtBQUssR0FBbEMsSUFBeUNQLFFBQVEsQ0FBQ0ksTUFBVCxDQUFnQixDQUFoQixNQUF1QixHQUFwRSxFQUF5RTtBQUN2RSxXQUFPLElBQVA7QUFDRDs7QUFFRCxTQUFPSixRQUFRLENBQUNhLEtBQVQsQ0FBZSxDQUFmLEVBQWtCRixVQUFsQixDQUFQO0FBQ0Q7QUFDRDs7Ozs7Ozs7QUFRQSxTQUFTRyxRQUFULENBQWtCUCxTQUFsQixFQUE2QlAsUUFBN0IsRUFBdUM7QUFDckNSLEVBQUFBLFVBQVUsQ0FBQ1EsUUFBRCxFQUFXLE1BQVgsRUFBbUIsUUFBbkIsQ0FBVjtBQUNBLE1BQUllLEtBQUssR0FBR2YsUUFBUSxDQUFDWSxXQUFULENBQXFCLEdBQXJCLENBQVo7O0FBRUEsTUFBSUcsS0FBSyxLQUFLLENBQUMsQ0FBWCxJQUFnQkEsS0FBSyxLQUFLLENBQTlCLEVBQWlDO0FBQy9CLFdBQU8sRUFBUDtBQUNELEdBTm9DLENBTW5DOzs7QUFHRixNQUFJQyxRQUFRLEdBQUdoQixRQUFRLENBQUNDLE1BQXhCOztBQUVBLE1BQUlELFFBQVEsQ0FBQ1UsUUFBVCxDQUFrQkgsU0FBbEIsQ0FBSixFQUFrQztBQUNoQ1MsSUFBQUEsUUFBUTtBQUNUOztBQUVELFNBQU9oQixRQUFRLENBQUNhLEtBQVQsQ0FBZUUsS0FBZixFQUFzQkMsUUFBdEIsQ0FBUDtBQUNEOztBQUVELFNBQVNDLHVCQUFULENBQWlDakIsUUFBakMsRUFBMkNlLEtBQTNDLEVBQWtEO0FBQ2hELE9BQUssSUFBSUcsQ0FBQyxHQUFHSCxLQUFiLEVBQW9CRyxDQUFDLElBQUksQ0FBekIsRUFBNEJBLENBQUMsRUFBN0IsRUFBaUM7QUFDL0IsUUFBSUMsSUFBSSxHQUFHbkIsUUFBUSxDQUFDRyxVQUFULENBQW9CZSxDQUFwQixDQUFYOztBQUVBLFFBQUlDLElBQUksS0FBSzlCLGNBQVQsSUFBMkI4QixJQUFJLEtBQUsvQixhQUF4QyxFQUF1RDtBQUNyRCxhQUFPOEIsQ0FBUDtBQUNEO0FBQ0Y7O0FBRUQsU0FBTyxDQUFDLENBQVI7QUFDRDtBQUNEOzs7Ozs7Ozs7QUFTQSxTQUFTRSxTQUFULENBQW1CYixTQUFuQixFQUE4QlAsUUFBOUIsRUFBd0NxQixHQUF4QyxFQUE2QztBQUMzQzdCLEVBQUFBLFVBQVUsQ0FBQ1EsUUFBRCxFQUFXLE1BQVgsRUFBbUIsUUFBbkIsQ0FBVjs7QUFFQSxNQUFJcUIsR0FBRyxLQUFLQyxTQUFaLEVBQXVCO0FBQ3JCOUIsSUFBQUEsVUFBVSxDQUFDNkIsR0FBRCxFQUFNLEtBQU4sRUFBYSxRQUFiLENBQVY7QUFDRDs7QUFFRCxNQUFJcEIsTUFBTSxHQUFHRCxRQUFRLENBQUNDLE1BQXRCOztBQUVBLE1BQUlBLE1BQU0sS0FBSyxDQUFmLEVBQWtCO0FBQ2hCLFdBQU8sRUFBUDtBQUNEOztBQUVELE1BQUlGLE9BQU8sR0FBR1EsU0FBUyxLQUFLLEdBQTVCO0FBQ0EsTUFBSVMsUUFBUSxHQUFHZixNQUFmLENBZDJDLENBY3BCOztBQUV2QixNQUFJc0IsWUFBWSxHQUFHdkIsUUFBUSxDQUFDRyxVQUFULENBQW9CRixNQUFNLEdBQUcsQ0FBN0IsQ0FBbkI7O0FBRUEsTUFBSXNCLFlBQVksS0FBS25DLGFBQWpCLElBQWtDLENBQUNXLE9BQUQsSUFBWXdCLFlBQVksS0FBS2xDLGNBQW5FLEVBQW1GO0FBQ2pGMkIsSUFBQUEsUUFBUTtBQUNULEdBcEIwQyxDQW9CekM7OztBQUdGLE1BQUlRLFNBQVMsR0FBRyxDQUFDLENBQWpCOztBQUVBLE1BQUl6QixPQUFKLEVBQWE7QUFDWHlCLElBQUFBLFNBQVMsR0FBR3hCLFFBQVEsQ0FBQ1ksV0FBVCxDQUFxQkwsU0FBckIsRUFBZ0NTLFFBQVEsR0FBRyxDQUEzQyxDQUFaO0FBQ0QsR0FGRCxNQUVPO0FBQ0w7QUFDQVEsSUFBQUEsU0FBUyxHQUFHUCx1QkFBdUIsQ0FBQ2pCLFFBQUQsRUFBV2dCLFFBQVEsR0FBRyxDQUF0QixDQUFuQyxDQUZLLENBRXdEOztBQUU3RCxRQUFJLENBQUNRLFNBQVMsS0FBSyxDQUFkLElBQW1CQSxTQUFTLEtBQUssQ0FBQyxDQUFuQyxLQUF5Q3hCLFFBQVEsQ0FBQ0ksTUFBVCxDQUFnQixDQUFoQixNQUF1QixHQUFoRSxJQUF1RWQsbUJBQW1CLENBQUNVLFFBQVEsQ0FBQ0csVUFBVCxDQUFvQixDQUFwQixDQUFELENBQTlGLEVBQXdIO0FBQ3RILGFBQU8sRUFBUDtBQUNEO0FBQ0YsR0FsQzBDLENBa0N6Qzs7O0FBR0YsTUFBSXNCLElBQUksR0FBR3pCLFFBQVEsQ0FBQ2EsS0FBVCxDQUFlVyxTQUFTLEdBQUcsQ0FBM0IsRUFBOEJSLFFBQTlCLENBQVgsQ0FyQzJDLENBcUNTOztBQUVwRCxNQUFJSyxHQUFHLEtBQUtDLFNBQVosRUFBdUI7QUFDckIsV0FBT0csSUFBUDtBQUNEOztBQUVELFNBQU9BLElBQUksQ0FBQ2YsUUFBTCxDQUFjVyxHQUFkLElBQXFCSSxJQUFJLENBQUNaLEtBQUwsQ0FBVyxDQUFYLEVBQWNZLElBQUksQ0FBQ3hCLE1BQUwsR0FBY29CLEdBQUcsQ0FBQ3BCLE1BQWhDLENBQXJCLEdBQStEd0IsSUFBdEU7QUFDRDtBQUNEOzs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxTQUFTQyxVQUFULENBQW9CbkIsU0FBcEIsRUFBK0JQLFFBQS9CLEVBQXlDO0FBQ3ZDUixFQUFBQSxVQUFVLENBQUNRLFFBQUQsRUFBVyxNQUFYLEVBQW1CLFFBQW5CLENBQVY7O0FBRUEsTUFBSUEsUUFBUSxDQUFDQyxNQUFULEtBQW9CLENBQXhCLEVBQTJCO0FBQ3pCLFdBQU8sR0FBUDtBQUNELEdBTHNDLENBS3JDOzs7QUFHRixNQUFJMEIsU0FBUyxHQUFHcEIsU0FBUyxLQUFLLElBQTlCOztBQUVBLE1BQUlvQixTQUFKLEVBQWU7QUFDYjNCLElBQUFBLFFBQVEsR0FBR0EsUUFBUSxDQUFDNEIsT0FBVCxDQUFpQixLQUFqQixFQUF3QnJCLFNBQXhCLENBQVg7QUFDRDs7QUFFRCxNQUFJc0IsVUFBVSxHQUFHN0IsUUFBUSxDQUFDOEIsVUFBVCxDQUFvQnZCLFNBQXBCLENBQWpCLENBZHVDLENBY1U7O0FBRWpELE1BQUl3QixLQUFLLEdBQUdGLFVBQVUsSUFBSUYsU0FBZCxJQUEyQjNCLFFBQVEsQ0FBQ0MsTUFBVCxHQUFrQixDQUE3QyxJQUFrREQsUUFBUSxDQUFDSSxNQUFULENBQWdCLENBQWhCLE1BQXVCLElBQXJGO0FBQ0EsTUFBSUssV0FBVyxHQUFHVCxRQUFRLENBQUNVLFFBQVQsQ0FBa0JILFNBQWxCLENBQWxCO0FBQ0EsTUFBSXlCLEtBQUssR0FBR2hDLFFBQVEsQ0FBQ2lDLEtBQVQsQ0FBZTFCLFNBQWYsQ0FBWjtBQUNBLE1BQUkyQixNQUFNLEdBQUcsRUFBYjtBQUNBLE1BQUlDLHlCQUF5QixHQUFHLElBQWhDO0FBQ0EsTUFBSUMsaUJBQWlCLEdBQUcsS0FBeEI7QUFDQSxNQUFJQyxjQUFjLEdBQUdmLFNBQXJCOztBQUVBLE1BQUk7QUFDRixTQUFLLElBQUlnQixTQUFTLEdBQUdOLEtBQUssQ0FBQ08sTUFBTSxDQUFDQyxRQUFSLENBQUwsRUFBaEIsRUFBMENDLEtBQS9DLEVBQXNELEVBQUVOLHlCQUF5QixHQUFHLENBQUNNLEtBQUssR0FBR0gsU0FBUyxDQUFDSSxJQUFWLEVBQVQsRUFBMkJDLElBQXpELENBQXRELEVBQXNIUix5QkFBeUIsR0FBRyxJQUFsSixFQUF3SjtBQUN0SixVQUFJUyxPQUFPLEdBQUdILEtBQUssQ0FBQ0ksS0FBcEI7O0FBRUEsVUFBSUQsT0FBTyxDQUFDM0MsTUFBUixLQUFtQixDQUFuQixJQUF3QjJDLE9BQU8sS0FBSyxHQUF4QyxFQUE2QztBQUMzQyxZQUFJQSxPQUFPLEtBQUssSUFBaEIsRUFBc0I7QUFDcEJWLFVBQUFBLE1BQU0sQ0FBQ1ksR0FBUCxHQURvQixDQUNOO0FBQ2YsU0FGRCxNQUVPO0FBQ0xaLFVBQUFBLE1BQU0sQ0FBQ2EsSUFBUCxDQUFZSCxPQUFaO0FBQ0Q7QUFDRjtBQUNGO0FBQ0YsR0FaRCxDQVlFLE9BQU9JLEdBQVAsRUFBWTtBQUNaWixJQUFBQSxpQkFBaUIsR0FBRyxJQUFwQjtBQUNBQyxJQUFBQSxjQUFjLEdBQUdXLEdBQWpCO0FBQ0QsR0FmRCxTQWVVO0FBQ1IsUUFBSTtBQUNGLFVBQUksQ0FBQ2IseUJBQUQsSUFBOEJHLFNBQVMsQ0FBQ1csTUFBVixJQUFvQixJQUF0RCxFQUE0RDtBQUMxRFgsUUFBQUEsU0FBUyxDQUFDVyxNQUFWO0FBQ0Q7QUFDRixLQUpELFNBSVU7QUFDUixVQUFJYixpQkFBSixFQUF1QjtBQUNyQixjQUFNQyxjQUFOO0FBQ0Q7QUFDRjtBQUNGOztBQUVELE1BQUlhLFVBQVUsR0FBR3JCLFVBQVUsR0FBR3RCLFNBQUgsR0FBZSxFQUExQztBQUNBMkMsRUFBQUEsVUFBVSxJQUFJaEIsTUFBTSxDQUFDaUIsSUFBUCxDQUFZNUMsU0FBWixDQUFkOztBQUVBLE1BQUlFLFdBQUosRUFBaUI7QUFDZnlDLElBQUFBLFVBQVUsSUFBSTNDLFNBQWQ7QUFDRDs7QUFFRCxNQUFJd0IsS0FBSixFQUFXO0FBQ1RtQixJQUFBQSxVQUFVLEdBQUcsT0FBT0EsVUFBcEI7QUFDRDs7QUFFRCxTQUFPQSxVQUFQO0FBQ0Q7QUFDRDs7Ozs7OztBQU9BLFNBQVNFLGFBQVQsQ0FBdUJSLE9BQXZCLEVBQWdDO0FBQzlCLE1BQUksT0FBT0EsT0FBUCxLQUFtQixRQUF2QixFQUFpQztBQUMvQixVQUFNLElBQUkvQyxTQUFKLENBQWUsbUNBQWtDK0MsT0FBUSxFQUF6RCxDQUFOO0FBQ0Q7QUFDRjtBQUNEOzs7Ozs7Ozs7OztBQVdBLFNBQVNTLEtBQVQsQ0FBZTlDLFNBQWYsRUFBMEIrQyxLQUExQixFQUFpQztBQUMvQixNQUFJcEIsTUFBTSxHQUFHLEVBQWIsQ0FEK0IsQ0FDZDs7QUFFakIsTUFBSXFCLDBCQUEwQixHQUFHLElBQWpDO0FBQ0EsTUFBSUMsa0JBQWtCLEdBQUcsS0FBekI7QUFDQSxNQUFJQyxlQUFlLEdBQUduQyxTQUF0Qjs7QUFFQSxNQUFJO0FBQ0YsU0FBSyxJQUFJb0MsVUFBVSxHQUFHSixLQUFLLENBQUNmLE1BQU0sQ0FBQ0MsUUFBUixDQUFMLEVBQWpCLEVBQTJDbUIsTUFBaEQsRUFBd0QsRUFBRUosMEJBQTBCLEdBQUcsQ0FBQ0ksTUFBTSxHQUFHRCxVQUFVLENBQUNoQixJQUFYLEVBQVYsRUFBNkJDLElBQTVELENBQXhELEVBQTJIWSwwQkFBMEIsR0FBRyxJQUF4SixFQUE4SjtBQUM1SixVQUFJWCxPQUFPLEdBQUdlLE1BQU0sQ0FBQ2QsS0FBckI7QUFDQU8sTUFBQUEsYUFBYSxDQUFDUixPQUFELENBQWI7O0FBRUEsVUFBSUEsT0FBTyxDQUFDM0MsTUFBUixLQUFtQixDQUF2QixFQUEwQjtBQUN4QmlDLFFBQUFBLE1BQU0sQ0FBQ2EsSUFBUCxDQUFZSCxPQUFaO0FBQ0Q7QUFDRjtBQUNGLEdBVEQsQ0FTRSxPQUFPSSxHQUFQLEVBQVk7QUFDWlEsSUFBQUEsa0JBQWtCLEdBQUcsSUFBckI7QUFDQUMsSUFBQUEsZUFBZSxHQUFHVCxHQUFsQjtBQUNELEdBWkQsU0FZVTtBQUNSLFFBQUk7QUFDRixVQUFJLENBQUNPLDBCQUFELElBQStCRyxVQUFVLENBQUNULE1BQVgsSUFBcUIsSUFBeEQsRUFBOEQ7QUFDNURTLFFBQUFBLFVBQVUsQ0FBQ1QsTUFBWDtBQUNEO0FBQ0YsS0FKRCxTQUlVO0FBQ1IsVUFBSU8sa0JBQUosRUFBd0I7QUFDdEIsY0FBTUMsZUFBTjtBQUNEO0FBQ0Y7QUFDRjs7QUFFRCxTQUFPL0IsVUFBVSxDQUFDbkIsU0FBRCxFQUFZMkIsTUFBTSxDQUFDaUIsSUFBUCxDQUFZNUMsU0FBWixDQUFaLENBQWpCO0FBQ0Q7QUFDRDs7Ozs7Ozs7O0FBU0EsU0FBU3FELFFBQVQsQ0FBa0JyRCxTQUFsQixFQUE2QitDLEtBQTdCLEVBQW9DO0FBQ2xDLE1BQUlPLFFBQVEsR0FBRyxFQUFmO0FBQ0EsTUFBSUMsT0FBTyxHQUFHLEtBQWQ7QUFDQSxNQUFJL0QsT0FBTyxHQUFHUSxTQUFTLEtBQUssR0FBNUIsQ0FIa0MsQ0FHRDs7QUFFakMsT0FBSyxJQUFJVyxDQUFDLEdBQUdvQyxLQUFLLENBQUNyRCxNQUFOLEdBQWUsQ0FBNUIsRUFBK0JpQixDQUFDLElBQUksQ0FBcEMsRUFBdUNBLENBQUMsRUFBeEMsRUFBNEM7QUFDMUMsUUFBSTBCLE9BQU8sR0FBR1UsS0FBSyxDQUFDcEMsQ0FBRCxDQUFuQjtBQUNBa0MsSUFBQUEsYUFBYSxDQUFDUixPQUFELENBQWI7O0FBRUEsUUFBSUEsT0FBTyxDQUFDM0MsTUFBUixLQUFtQixDQUF2QixFQUEwQjtBQUN4QixlQUR3QixDQUNkO0FBQ1g7O0FBRUQ0RCxJQUFBQSxRQUFRLEdBQUdqQixPQUFPLEdBQUdyQyxTQUFWLEdBQXNCc0QsUUFBakMsQ0FSMEMsQ0FRQzs7QUFFM0MsUUFBSS9ELFdBQVcsQ0FBQ0MsT0FBRCxFQUFVNkMsT0FBVixDQUFmLEVBQW1DO0FBQ2pDO0FBQ0FrQixNQUFBQSxPQUFPLEdBQUcsSUFBVjtBQUNBO0FBQ0Q7QUFDRixHQXBCaUMsQ0FvQmhDOzs7QUFHRixNQUFJLENBQUNBLE9BQUwsRUFBYztBQUNaRCxJQUFBQSxRQUFRLEdBQUdFLE9BQU8sQ0FBQ0MsR0FBUixLQUFnQnpELFNBQWhCLEdBQTRCc0QsUUFBdkM7QUFDRDs7QUFFRCxNQUFJWCxVQUFVLEdBQUd4QixVQUFVLENBQUNuQixTQUFELEVBQVlzRCxRQUFaLENBQTNCOztBQUVBLE1BQUlYLFVBQVUsQ0FBQzlDLE1BQVgsQ0FBa0I4QyxVQUFVLENBQUNqRCxNQUFYLEdBQW9CLENBQXRDLE1BQTZDTSxTQUFqRCxFQUE0RDtBQUMxRDtBQUNBO0FBQ0EsUUFBSSxDQUFDUixPQUFELElBQVltRCxVQUFVLENBQUNqRCxNQUFYLEtBQXNCLENBQWxDLElBQXVDaUQsVUFBVSxDQUFDOUMsTUFBWCxDQUFrQixDQUFsQixNQUF5QixHQUFoRSxJQUF1RWQsbUJBQW1CLENBQUM0RCxVQUFVLENBQUMvQyxVQUFYLENBQXNCLENBQXRCLENBQUQsQ0FBOUYsRUFBMEg7QUFDeEgsYUFBTytDLFVBQVA7QUFDRCxLQUx5RCxDQUt4RDs7O0FBR0YsV0FBT0EsVUFBVSxDQUFDckMsS0FBWCxDQUFpQixDQUFqQixFQUFvQnFDLFVBQVUsQ0FBQ2pELE1BQVgsR0FBb0IsQ0FBeEMsQ0FBUDtBQUNEOztBQUVELFNBQU9pRCxVQUFQO0FBQ0Q7QUFDRDs7Ozs7Ozs7Ozs7Ozs7O0FBZUEsU0FBU2UsU0FBVCxDQUFtQjFELFNBQW5CLEVBQThCMkQsSUFBOUIsRUFBb0NDLEVBQXBDLEVBQXdDO0FBQ3RDM0UsRUFBQUEsVUFBVSxDQUFDMEUsSUFBRCxFQUFPLE1BQVAsRUFBZSxRQUFmLENBQVY7QUFDQTFFLEVBQUFBLFVBQVUsQ0FBQzJFLEVBQUQsRUFBSyxJQUFMLEVBQVcsUUFBWCxDQUFWOztBQUVBLE1BQUlELElBQUksS0FBS0MsRUFBYixFQUFpQjtBQUNmLFdBQU8sRUFBUDtBQUNEOztBQUVERCxFQUFBQSxJQUFJLEdBQUdOLFFBQVEsQ0FBQ3JELFNBQUQsRUFBWSxDQUFDMkQsSUFBRCxDQUFaLENBQWY7QUFDQUMsRUFBQUEsRUFBRSxHQUFHUCxRQUFRLENBQUNyRCxTQUFELEVBQVksQ0FBQzRELEVBQUQsQ0FBWixDQUFiOztBQUVBLE1BQUlELElBQUksS0FBS0MsRUFBYixFQUFpQjtBQUNmLFdBQU8sRUFBUDtBQUNELEdBYnFDLENBYXBDO0FBQ0Y7QUFDQTs7O0FBR0EsTUFBSUMsT0FBTyxHQUFHLENBQWQ7QUFDQSxNQUFJQyxhQUFhLEdBQUcsRUFBcEI7O0FBRUEsU0FBTyxJQUFQLEVBQWE7QUFDWCxRQUFJRixFQUFFLENBQUNyQyxVQUFILENBQWNvQyxJQUFkLENBQUosRUFBeUI7QUFDdkI7QUFDQUcsTUFBQUEsYUFBYSxHQUFHRixFQUFFLENBQUN0RCxLQUFILENBQVNxRCxJQUFJLENBQUNqRSxNQUFkLENBQWhCO0FBQ0E7QUFDRCxLQUxVLENBS1Q7OztBQUdGaUUsSUFBQUEsSUFBSSxHQUFHNUQsUUFBUSxDQUFDQyxTQUFELEVBQVkyRCxJQUFaLENBQWY7QUFDQUUsSUFBQUEsT0FBTztBQUNSLEdBL0JxQyxDQStCcEM7OztBQUdGLE1BQUlDLGFBQWEsQ0FBQ3BFLE1BQWQsR0FBdUIsQ0FBM0IsRUFBOEI7QUFDNUJvRSxJQUFBQSxhQUFhLEdBQUdBLGFBQWEsQ0FBQ3hELEtBQWQsQ0FBb0IsQ0FBcEIsQ0FBaEI7QUFDRDs7QUFFRCxTQUFPLENBQUMsT0FBT04sU0FBUixFQUFtQitELE1BQW5CLENBQTBCRixPQUExQixJQUFxQ0MsYUFBNUM7QUFDRDtBQUNEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFrQkEsU0FBU0UsTUFBVCxDQUFnQmhFLFNBQWhCLEVBQTJCUCxRQUEzQixFQUFxQztBQUNuQ1IsRUFBQUEsVUFBVSxDQUFDUSxRQUFELEVBQVcsTUFBWCxFQUFtQixRQUFuQixDQUFWO0FBQ0EsTUFBSWtDLE1BQU0sR0FBRztBQUNYc0MsSUFBQUEsSUFBSSxFQUFFLEVBREs7QUFFWEMsSUFBQUEsR0FBRyxFQUFFLEVBRk07QUFHWGhELElBQUFBLElBQUksRUFBRSxFQUhLO0FBSVhKLElBQUFBLEdBQUcsRUFBRSxFQUpNO0FBS1gzQixJQUFBQSxJQUFJLEVBQUUsRUFMSyxFQUFiOztBQU9BLE1BQUlPLE1BQU0sR0FBR0QsUUFBUSxDQUFDQyxNQUF0Qjs7QUFFQSxNQUFJQSxNQUFNLEtBQUssQ0FBZixFQUFrQjtBQUNoQixXQUFPaUMsTUFBUDtBQUNELEdBYmtDLENBYWpDOzs7QUFHRkEsRUFBQUEsTUFBTSxDQUFDVCxJQUFQLEdBQWNMLFNBQVMsQ0FBQ2IsU0FBRCxFQUFZUCxRQUFaLENBQXZCO0FBQ0FrQyxFQUFBQSxNQUFNLENBQUNiLEdBQVAsR0FBYVAsUUFBUSxDQUFDUCxTQUFELEVBQVkyQixNQUFNLENBQUNULElBQW5CLENBQXJCO0FBQ0EsTUFBSWlELFVBQVUsR0FBR3hDLE1BQU0sQ0FBQ1QsSUFBUCxDQUFZeEIsTUFBN0I7QUFDQWlDLEVBQUFBLE1BQU0sQ0FBQ3hDLElBQVAsR0FBY3dDLE1BQU0sQ0FBQ1QsSUFBUCxDQUFZWixLQUFaLENBQWtCLENBQWxCLEVBQXFCNkQsVUFBVSxHQUFHeEMsTUFBTSxDQUFDYixHQUFQLENBQVdwQixNQUE3QyxDQUFkO0FBQ0EsTUFBSTBFLFVBQVUsR0FBR0QsVUFBVSxLQUFLLENBQWYsR0FBbUIsQ0FBbkIsR0FBdUJBLFVBQVUsR0FBRyxDQUFyRDtBQUNBeEMsRUFBQUEsTUFBTSxDQUFDdUMsR0FBUCxHQUFhekUsUUFBUSxDQUFDYSxLQUFULENBQWUsQ0FBZixFQUFrQmIsUUFBUSxDQUFDQyxNQUFULEdBQWtCMEUsVUFBcEMsQ0FBYixDQXJCbUMsQ0FxQjJCOztBQUU5RCxNQUFJQyxhQUFhLEdBQUc1RSxRQUFRLENBQUNHLFVBQVQsQ0FBb0IsQ0FBcEIsQ0FBcEIsQ0F2Qm1DLENBdUJTOztBQUU1QyxNQUFJeUUsYUFBYSxLQUFLeEYsYUFBdEIsRUFBcUM7QUFDbkM4QyxJQUFBQSxNQUFNLENBQUNzQyxJQUFQLEdBQWMsR0FBZDtBQUNBLFdBQU90QyxNQUFQO0FBQ0QsR0E1QmtDLENBNEJqQzs7O0FBR0YsTUFBSTNCLFNBQVMsS0FBSyxHQUFsQixFQUF1QjtBQUNyQixXQUFPMkIsTUFBUDtBQUNELEdBakNrQyxDQWlDakM7OztBQUdGLE1BQUkwQyxhQUFhLEtBQUt2RixjQUF0QixFQUFzQztBQUNwQztBQUNBO0FBQ0E2QyxJQUFBQSxNQUFNLENBQUNzQyxJQUFQLEdBQWMsSUFBZDtBQUNBLFdBQU90QyxNQUFQO0FBQ0QsR0F6Q2tDLENBeUNqQzs7O0FBR0YsTUFBSWpDLE1BQU0sR0FBRyxDQUFULElBQWNYLG1CQUFtQixDQUFDc0YsYUFBRCxDQUFqQyxJQUFvRDVFLFFBQVEsQ0FBQ0ksTUFBVCxDQUFnQixDQUFoQixNQUF1QixHQUEvRSxFQUFvRjtBQUNsRixRQUFJSCxNQUFNLEdBQUcsQ0FBYixFQUFnQjtBQUNkO0FBQ0EsVUFBSTRFLGFBQWEsR0FBRzdFLFFBQVEsQ0FBQ0csVUFBVCxDQUFvQixDQUFwQixDQUFwQjs7QUFFQSxVQUFJMEUsYUFBYSxLQUFLekYsYUFBbEIsSUFBbUN5RixhQUFhLEtBQUt4RixjQUF6RCxFQUF5RTtBQUN2RTZDLFFBQUFBLE1BQU0sQ0FBQ3NDLElBQVAsR0FBY3hFLFFBQVEsQ0FBQ2EsS0FBVCxDQUFlLENBQWYsRUFBa0IsQ0FBbEIsQ0FBZDtBQUNBLGVBQU9xQixNQUFQO0FBQ0Q7QUFDRixLQVRpRixDQVNoRjs7O0FBR0ZBLElBQUFBLE1BQU0sQ0FBQ3NDLElBQVAsR0FBY3hFLFFBQVEsQ0FBQ2EsS0FBVCxDQUFlLENBQWYsRUFBa0IsQ0FBbEIsQ0FBZDtBQUNEOztBQUVELFNBQU9xQixNQUFQO0FBQ0Q7QUFDRDs7Ozs7Ozs7Ozs7Ozs7O0FBZUEsU0FBUzRDLE9BQVQsQ0FBaUJ2RSxTQUFqQixFQUE0QndFLFVBQTVCLEVBQXdDO0FBQ3RDdkYsRUFBQUEsVUFBVSxDQUFDdUYsVUFBRCxFQUFhLFlBQWIsRUFBMkIsUUFBM0IsQ0FBVjtBQUNBLE1BQUl0RCxJQUFJLEdBQUdzRCxVQUFVLENBQUN0RCxJQUFYLElBQW9CLEdBQUVzRCxVQUFVLENBQUNyRixJQUFYLElBQW1CLEVBQUcsR0FBRXFGLFVBQVUsQ0FBQzFELEdBQVgsSUFBa0IsRUFBRyxFQUE5RSxDQUZzQyxDQUUyQztBQUNqRjs7QUFFQSxNQUFJLENBQUMwRCxVQUFVLENBQUNOLEdBQVosSUFBbUJNLFVBQVUsQ0FBQ04sR0FBWCxLQUFtQk0sVUFBVSxDQUFDUCxJQUFyRCxFQUEyRDtBQUN6RCxXQUFRLEdBQUVPLFVBQVUsQ0FBQ1AsSUFBWCxJQUFtQixFQUFHLEdBQUUvQyxJQUFLLEVBQXZDO0FBQ0QsR0FQcUMsQ0FPcEM7OztBQUdGLFNBQVEsR0FBRXNELFVBQVUsQ0FBQ04sR0FBSSxHQUFFbEUsU0FBVSxHQUFFa0IsSUFBSyxFQUE1QztBQUNEO0FBQ0Q7Ozs7Ozs7OztBQVNBLFNBQVN1RCxnQkFBVCxDQUEwQmhGLFFBQTFCLEVBQW9DO0FBQ2xDLE1BQUksT0FBT0EsUUFBUCxLQUFvQixRQUF4QixFQUFrQztBQUNoQyxXQUFPQSxRQUFQO0FBQ0Q7O0FBRUQsTUFBSUEsUUFBUSxDQUFDQyxNQUFULEtBQW9CLENBQXhCLEVBQTJCO0FBQ3pCLFdBQU8sRUFBUDtBQUNEOztBQUVELE1BQUlnRixZQUFZLEdBQUdyQixRQUFRLENBQUMsSUFBRCxFQUFPLENBQUM1RCxRQUFELENBQVAsQ0FBM0I7O0FBRUEsTUFBSUMsTUFBTSxHQUFHZ0YsWUFBWSxDQUFDaEYsTUFBMUI7O0FBRUEsTUFBSUEsTUFBTSxHQUFHLENBQWIsRUFBZ0I7QUFDZDtBQUNBLFdBQU9ELFFBQVA7QUFDRDs7QUFFRCxNQUFJNEUsYUFBYSxHQUFHSyxZQUFZLENBQUM5RSxVQUFiLENBQXdCLENBQXhCLENBQXBCLENBbEJrQyxDQWtCYzs7QUFFaEQsTUFBSXlFLGFBQWEsS0FBS3ZGLGNBQWxCLElBQW9DNEYsWUFBWSxDQUFDN0UsTUFBYixDQUFvQixDQUFwQixNQUEyQixJQUFuRSxFQUF5RTtBQUN2RTtBQUNBLFFBQUlILE1BQU0sSUFBSSxDQUFkLEVBQWlCO0FBQ2YsVUFBSUksU0FBUyxHQUFHNEUsWUFBWSxDQUFDN0UsTUFBYixDQUFvQixDQUFwQixDQUFoQjs7QUFFQSxVQUFJQyxTQUFTLEtBQUssR0FBZCxJQUFxQkEsU0FBUyxLQUFLLEdBQXZDLEVBQTRDO0FBQzFDLGVBQU9MLFFBQVA7QUFDRDtBQUNGOztBQUVELFdBQU8saUJBQWlCaUYsWUFBWSxDQUFDcEUsS0FBYixDQUFtQixDQUFuQixDQUF4QjtBQUNELEdBWEQsTUFXTyxJQUFJdkIsbUJBQW1CLENBQUNzRixhQUFELENBQW5CLElBQXNDSyxZQUFZLENBQUM3RSxNQUFiLENBQW9CLENBQXBCLE1BQTJCLEdBQXJFLEVBQTBFO0FBQy9FLFdBQU8sWUFBWTZFLFlBQW5CO0FBQ0Q7O0FBRUQsU0FBT2pGLFFBQVA7QUFDRDs7QUFFRCxJQUFJa0YsU0FBUyxHQUFHO0FBQ2RDLEVBQUFBLEdBQUcsRUFBRSxJQURTO0FBRWRDLEVBQUFBLFNBQVMsRUFBRSxHQUZHO0FBR2RDLEVBQUFBLFFBQVEsRUFBRSxTQUFTQSxRQUFULENBQWtCckYsUUFBbEIsRUFBNEJxQixHQUE1QixFQUFpQztBQUN6QyxXQUFPRCxTQUFTLENBQUMsS0FBSytELEdBQU4sRUFBV25GLFFBQVgsRUFBcUJxQixHQUFyQixDQUFoQjtBQUNELEdBTGE7QUFNZGlFLEVBQUFBLFNBQVMsRUFBRSxTQUFTQSxTQUFULENBQW1CdEYsUUFBbkIsRUFBNkI7QUFDdEMsV0FBTzBCLFVBQVUsQ0FBQyxLQUFLeUQsR0FBTixFQUFXbkYsUUFBWCxDQUFqQjtBQUNELEdBUmE7QUFTZG1ELEVBQUFBLElBQUksRUFBRSxTQUFTQSxJQUFULEdBQWdCO0FBQ3BCLFNBQUssSUFBSW9DLElBQUksR0FBR0MsU0FBUyxDQUFDdkYsTUFBckIsRUFBNkJxRCxLQUFLLEdBQUcsSUFBSW1DLEtBQUosQ0FBVUYsSUFBVixDQUFyQyxFQUFzREcsSUFBSSxHQUFHLENBQWxFLEVBQXFFQSxJQUFJLEdBQUdILElBQTVFLEVBQWtGRyxJQUFJLEVBQXRGLEVBQTBGO0FBQ3hGcEMsTUFBQUEsS0FBSyxDQUFDb0MsSUFBRCxDQUFMLEdBQWNGLFNBQVMsQ0FBQ0UsSUFBRCxDQUF2QjtBQUNEOztBQUVELFdBQU9yQyxLQUFLLENBQUMsS0FBSzhCLEdBQU4sRUFBVzdCLEtBQVgsQ0FBWjtBQUNELEdBZmE7QUFnQmRxQyxFQUFBQSxPQUFPLEVBQUUsU0FBU0EsT0FBVCxDQUFpQjNGLFFBQWpCLEVBQTJCO0FBQ2xDLFdBQU9jLFFBQVEsQ0FBQyxLQUFLcUUsR0FBTixFQUFXbkYsUUFBWCxDQUFmO0FBQ0QsR0FsQmE7QUFtQmQ0RixFQUFBQSxPQUFPLEVBQUUsU0FBU0EsT0FBVCxDQUFpQjVGLFFBQWpCLEVBQTJCO0FBQ2xDLFdBQU9NLFFBQVEsQ0FBQyxLQUFLNkUsR0FBTixFQUFXbkYsUUFBWCxDQUFmO0FBQ0QsR0FyQmE7QUFzQmQ2RixFQUFBQSxVQUFVLEVBQUUsU0FBU0EsVUFBVCxDQUFvQjdGLFFBQXBCLEVBQThCO0FBQ3hDLFdBQU9GLFdBQVcsQ0FBQyxLQUFELEVBQVFFLFFBQVIsQ0FBbEI7QUFDRCxHQXhCYTtBQXlCZDhGLEVBQUFBLFFBQVEsRUFBRSxTQUFTQSxRQUFULENBQWtCNUIsSUFBbEIsRUFBd0JDLEVBQXhCLEVBQTRCO0FBQ3BDLFdBQU9GLFNBQVMsQ0FBQyxLQUFLa0IsR0FBTixFQUFXakIsSUFBWCxFQUFpQkMsRUFBakIsQ0FBaEI7QUFDRCxHQTNCYTtBQTRCZDRCLEVBQUFBLE9BQU8sRUFBRSxTQUFTQSxPQUFULEdBQW1CO0FBQzFCLFNBQUssSUFBSUMsS0FBSyxHQUFHUixTQUFTLENBQUN2RixNQUF0QixFQUE4QnFELEtBQUssR0FBRyxJQUFJbUMsS0FBSixDQUFVTyxLQUFWLENBQXRDLEVBQXdEQyxLQUFLLEdBQUcsQ0FBckUsRUFBd0VBLEtBQUssR0FBR0QsS0FBaEYsRUFBdUZDLEtBQUssRUFBNUYsRUFBZ0c7QUFDOUYzQyxNQUFBQSxLQUFLLENBQUMyQyxLQUFELENBQUwsR0FBZVQsU0FBUyxDQUFDUyxLQUFELENBQXhCO0FBQ0Q7O0FBRUQsV0FBT3JDLFFBQVEsQ0FBQyxLQUFLdUIsR0FBTixFQUFXN0IsS0FBWCxDQUFmO0FBQ0QsR0FsQ2E7QUFtQ2Q0QyxFQUFBQSxLQUFLLEVBQUUsU0FBU0EsS0FBVCxDQUFlbEcsUUFBZixFQUF5QjtBQUM5QixXQUFPdUUsTUFBTSxDQUFDLEtBQUtZLEdBQU4sRUFBV25GLFFBQVgsQ0FBYjtBQUNELEdBckNhO0FBc0NkbUcsRUFBQUEsTUFBTSxFQUFFLFNBQVNBLE1BQVQsQ0FBZ0JwQixVQUFoQixFQUE0QjtBQUNsQyxXQUFPRCxPQUFPLENBQUMsS0FBS0ssR0FBTixFQUFXSixVQUFYLENBQWQ7QUFDRCxHQXhDYTtBQXlDZEMsRUFBQUEsZ0JBQWdCLEVBQUVBLGdCQXpDSixFQUFoQjs7QUEyQ0EsSUFBSW9CLFNBQVMsR0FBRztBQUNkakIsRUFBQUEsR0FBRyxFQUFFLEdBRFM7QUFFZEMsRUFBQUEsU0FBUyxFQUFFLEdBRkc7QUFHZEMsRUFBQUEsUUFBUSxFQUFFLFNBQVNBLFFBQVQsQ0FBa0JyRixRQUFsQixFQUE0QnFCLEdBQTVCLEVBQWlDO0FBQ3pDLFdBQU9ELFNBQVMsQ0FBQyxLQUFLK0QsR0FBTixFQUFXbkYsUUFBWCxFQUFxQnFCLEdBQXJCLENBQWhCO0FBQ0QsR0FMYTtBQU1kaUUsRUFBQUEsU0FBUyxFQUFFLFNBQVNBLFNBQVQsQ0FBbUJ0RixRQUFuQixFQUE2QjtBQUN0QyxXQUFPMEIsVUFBVSxDQUFDLEtBQUt5RCxHQUFOLEVBQVduRixRQUFYLENBQWpCO0FBQ0QsR0FSYTtBQVNkbUQsRUFBQUEsSUFBSSxFQUFFLFNBQVNBLElBQVQsR0FBZ0I7QUFDcEIsU0FBSyxJQUFJa0QsS0FBSyxHQUFHYixTQUFTLENBQUN2RixNQUF0QixFQUE4QnFELEtBQUssR0FBRyxJQUFJbUMsS0FBSixDQUFVWSxLQUFWLENBQXRDLEVBQXdEQyxLQUFLLEdBQUcsQ0FBckUsRUFBd0VBLEtBQUssR0FBR0QsS0FBaEYsRUFBdUZDLEtBQUssRUFBNUYsRUFBZ0c7QUFDOUZoRCxNQUFBQSxLQUFLLENBQUNnRCxLQUFELENBQUwsR0FBZWQsU0FBUyxDQUFDYyxLQUFELENBQXhCO0FBQ0Q7O0FBRUQsV0FBT2pELEtBQUssQ0FBQyxLQUFLOEIsR0FBTixFQUFXN0IsS0FBWCxDQUFaO0FBQ0QsR0FmYTtBQWdCZHFDLEVBQUFBLE9BQU8sRUFBRSxTQUFTQSxPQUFULENBQWlCM0YsUUFBakIsRUFBMkI7QUFDbEMsV0FBT2MsUUFBUSxDQUFDLEtBQUtxRSxHQUFOLEVBQVduRixRQUFYLENBQWY7QUFDRCxHQWxCYTtBQW1CZDRGLEVBQUFBLE9BQU8sRUFBRSxTQUFTQSxPQUFULENBQWlCNUYsUUFBakIsRUFBMkI7QUFDbEMsV0FBT00sUUFBUSxDQUFDLEtBQUs2RSxHQUFOLEVBQVduRixRQUFYLENBQWY7QUFDRCxHQXJCYTtBQXNCZDZGLEVBQUFBLFVBQVUsRUFBRSxTQUFTQSxVQUFULENBQW9CN0YsUUFBcEIsRUFBOEI7QUFDeEMsV0FBT0YsV0FBVyxDQUFDLElBQUQsRUFBT0UsUUFBUCxDQUFsQjtBQUNELEdBeEJhO0FBeUJkOEYsRUFBQUEsUUFBUSxFQUFFLFNBQVNBLFFBQVQsQ0FBa0I1QixJQUFsQixFQUF3QkMsRUFBeEIsRUFBNEI7QUFDcEMsV0FBT0YsU0FBUyxDQUFDLEtBQUtrQixHQUFOLEVBQVdqQixJQUFYLEVBQWlCQyxFQUFqQixDQUFoQjtBQUNELEdBM0JhO0FBNEJkNEIsRUFBQUEsT0FBTyxFQUFFLFNBQVNBLE9BQVQsR0FBbUI7QUFDMUIsU0FBSyxJQUFJUSxLQUFLLEdBQUdmLFNBQVMsQ0FBQ3ZGLE1BQXRCLEVBQThCcUQsS0FBSyxHQUFHLElBQUltQyxLQUFKLENBQVVjLEtBQVYsQ0FBdEMsRUFBd0RDLEtBQUssR0FBRyxDQUFyRSxFQUF3RUEsS0FBSyxHQUFHRCxLQUFoRixFQUF1RkMsS0FBSyxFQUE1RixFQUFnRztBQUM5RmxELE1BQUFBLEtBQUssQ0FBQ2tELEtBQUQsQ0FBTCxHQUFlaEIsU0FBUyxDQUFDZ0IsS0FBRCxDQUF4QjtBQUNEOztBQUVELFdBQU81QyxRQUFRLENBQUMsS0FBS3VCLEdBQU4sRUFBVzdCLEtBQVgsQ0FBZjtBQUNELEdBbENhO0FBbUNkNEMsRUFBQUEsS0FBSyxFQUFFLFNBQVNBLEtBQVQsQ0FBZWxHLFFBQWYsRUFBeUI7QUFDOUIsV0FBT3VFLE1BQU0sQ0FBQyxLQUFLWSxHQUFOLEVBQVduRixRQUFYLENBQWI7QUFDRCxHQXJDYTtBQXNDZG1HLEVBQUFBLE1BQU0sRUFBRSxTQUFTQSxNQUFULENBQWdCcEIsVUFBaEIsRUFBNEI7QUFDbEMsV0FBT0QsT0FBTyxDQUFDLEtBQUtLLEdBQU4sRUFBV0osVUFBWCxDQUFkO0FBQ0QsR0F4Q2E7QUF5Q2RDLEVBQUFBLGdCQUFnQixFQUFFLFNBQVNBLGdCQUFULENBQTBCaEYsUUFBMUIsRUFBb0M7QUFDcEQsV0FBT0EsUUFBUCxDQURvRCxDQUNuQztBQUNsQixHQTNDYSxFQUFoQjs7QUE2Q0EsSUFBSXlHLElBQUksR0FBR3pILE9BQU8sR0FBR2tHLFNBQUgsR0FBZWtCLFNBQWpDO0FBQ0FLLElBQUksQ0FBQ0MsS0FBTCxHQUFheEIsU0FBYjtBQUNBdUIsSUFBSSxDQUFDRSxLQUFMLEdBQWFQLFNBQWI7QUFDQVEsTUFBTSxDQUFDQyxPQUFQLEdBQWlCSixJQUFqQiIsInNvdXJjZXNDb250ZW50IjpbIid1c2Ugc3RyaWN0JztcblxudmFyIGlzV2luMzIgPSBUaS5QbGF0Zm9ybS5vc25hbWUgPT09ICd3aW5kb3dzcGhvbmUnIHx8IFRpLlBsYXRmb3JtLm9zbmFtZSA9PT0gJ3dpbmRvd3NzdG9yZSc7XG52YXIgRk9SV0FSRF9TTEFTSCA9IDQ3OyAvLyAnLydcblxudmFyIEJBQ0tXQVJEX1NMQVNIID0gOTI7IC8vICdcXFxcJ1xuXG4vKipcbiAqIElzIHRoaXMgW2EtekEtWl0/XG4gKiBAcGFyYW0gIHtudW1iZXJ9ICBjaGFyQ29kZSB2YWx1ZSBmcm9tIFN0cmluZy5jaGFyQ29kZUF0KClcbiAqIEByZXR1cm4ge0Jvb2xlYW59ICAgICAgICAgIFtkZXNjcmlwdGlvbl1cbiAqL1xuXG5mdW5jdGlvbiBpc1dpbmRvd3NEZXZpY2VOYW1lKGNoYXJDb2RlKSB7XG4gIHJldHVybiBjaGFyQ29kZSA+PSA2NSAmJiBjaGFyQ29kZSA8PSA5MCB8fCBjaGFyQ29kZSA+PSA5NyAmJiBjaGFyQ29kZSA8PSAxMjI7XG59XG4vKipcbiAqIFthc3NlcnRUeXBlIGRlc2NyaXB0aW9uXVxuICogQHBhcmFtICB7Kn0gYXJnICAgICAgcGFzc2VkIGluIGFyZ3VtZW50IHZhbHVlXG4gKiBAcGFyYW0gIHtzdHJpbmd9IG5hbWUgICAgIG5hbWUgb2YgdGhlIGFyZ3VtZW50XG4gKiBAcGFyYW0gIHtzdHJpbmd9IHR5cGVuYW1lIGkuZS4gJ3N0cmluZydcbiAqIEByZXR1cm4ge3ZvaWR9XG4gKi9cblxuXG5mdW5jdGlvbiBhc3NlcnRUeXBlKGFyZywgbmFtZSwgdHlwZW5hbWUpIHtcbiAgdmFyIHR5cGUgPSB0eXBlb2YgYXJnO1xuXG4gIGlmICh0eXBlICE9PSB0eXBlbmFtZSkge1xuICAgIHRocm93IG5ldyBUeXBlRXJyb3IoYFRoZSBcIiR7bmFtZX1cIiBhcmd1bWVudCBtdXN0IGJlIG9mIHR5cGUgJHt0eXBlbmFtZX0uIFJlY2VpdmVkIHR5cGUgJHt0eXBlfWApO1xuICB9XG59XG4vKipcbiAqIFtpc0Fic29sdXRlIGRlc2NyaXB0aW9uXVxuICogQHBhcmFtICB7Ym9vbGVhbn0gaXNQb3NpeCB3aGV0aGVyIHRoaXMgaW1wbCBpcyBmb3IgUE9TSVggb3Igbm90XG4gKiBAcGFyYW0gIHtzdHJpbmd9IGZpbGVwYXRoICAgaW5wdXQgZmlsZSBwYXRoXG4gKiBAcmV0dXJuIHtCb29sZWFufSAgICAgICAgICBbZGVzY3JpcHRpb25dXG4gKi9cblxuXG5mdW5jdGlvbiBfaXNBYnNvbHV0ZShpc1Bvc2l4LCBmaWxlcGF0aCkge1xuICBhc3NlcnRUeXBlKGZpbGVwYXRoLCAncGF0aCcsICdzdHJpbmcnKTtcbiAgdmFyIGxlbmd0aCA9IGZpbGVwYXRoLmxlbmd0aDsgLy8gZW1wdHkgc3RyaW5nIHNwZWNpYWwgY2FzZVxuXG4gIGlmIChsZW5ndGggPT09IDApIHtcbiAgICByZXR1cm4gZmFsc2U7XG4gIH1cblxuICB2YXIgZmlyc3RDaGFyID0gZmlsZXBhdGguY2hhckNvZGVBdCgwKTtcblxuICBpZiAoZmlyc3RDaGFyID09PSBGT1JXQVJEX1NMQVNIKSB7XG4gICAgcmV0dXJuIHRydWU7XG4gIH0gLy8gd2UgYWxyZWFkeSBkaWQgb3VyIGNoZWNrcyBmb3IgcG9zaXhcblxuXG4gIGlmIChpc1Bvc2l4KSB7XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9IC8vIHdpbjMyIGZyb20gaGVyZSBvbiBvdXRcblxuXG4gIGlmIChmaXJzdENoYXIgPT09IEJBQ0tXQVJEX1NMQVNIKSB7XG4gICAgcmV0dXJuIHRydWU7XG4gIH1cblxuICBpZiAobGVuZ3RoID4gMiAmJiBpc1dpbmRvd3NEZXZpY2VOYW1lKGZpcnN0Q2hhcikgJiYgZmlsZXBhdGguY2hhckF0KDEpID09PSAnOicpIHtcbiAgICB2YXIgdGhpcmRDaGFyID0gZmlsZXBhdGguY2hhckF0KDIpO1xuICAgIHJldHVybiB0aGlyZENoYXIgPT09ICcvJyB8fCB0aGlyZENoYXIgPT09ICdcXFxcJztcbiAgfVxuXG4gIHJldHVybiBmYWxzZTtcbn1cbi8qKlxuICogW2Rpcm5hbWUgZGVzY3JpcHRpb25dXG4gKiBAcGFyYW0gIHtzdHJpbmd9IHNlcGFyYXRvciAgcGxhdGZvcm0tc3BlY2lmaWMgZmlsZSBzZXBhcmF0b3JcbiAqIEBwYXJhbSAge3N0cmluZ30gZmlsZXBhdGggICBpbnB1dCBmaWxlIHBhdGhcbiAqIEByZXR1cm4ge3N0cmluZ30gICAgICAgICAgICBbZGVzY3JpcHRpb25dXG4gKi9cblxuXG5mdW5jdGlvbiBfZGlybmFtZShzZXBhcmF0b3IsIGZpbGVwYXRoKSB7XG4gIGFzc2VydFR5cGUoZmlsZXBhdGgsICdwYXRoJywgJ3N0cmluZycpO1xuICB2YXIgbGVuZ3RoID0gZmlsZXBhdGgubGVuZ3RoO1xuXG4gIGlmIChsZW5ndGggPT09IDApIHtcbiAgICByZXR1cm4gJy4nO1xuICB9IC8vIGlnbm9yZSB0cmFpbGluZyBzZXBhcmF0b3JcblxuXG4gIHZhciBmcm9tSW5kZXggPSBsZW5ndGggLSAxO1xuICB2YXIgaGFkVHJhaWxpbmcgPSBmaWxlcGF0aC5lbmRzV2l0aChzZXBhcmF0b3IpO1xuXG4gIGlmIChoYWRUcmFpbGluZykge1xuICAgIGZyb21JbmRleC0tO1xuICB9XG5cbiAgdmFyIGZvdW5kSW5kZXggPSBmaWxlcGF0aC5sYXN0SW5kZXhPZihzZXBhcmF0b3IsIGZyb21JbmRleCk7IC8vIG5vIHNlcGFyYXRvcnNcblxuICBpZiAoZm91bmRJbmRleCA9PT0gLTEpIHtcbiAgICAvLyBoYW5kbGUgc3BlY2lhbCBjYXNlIG9mIHJvb3Qgd2luZG93cyBwYXRoc1xuICAgIGlmIChsZW5ndGggPj0gMiAmJiBzZXBhcmF0b3IgPT09ICdcXFxcJyAmJiBmaWxlcGF0aC5jaGFyQXQoMSkgPT09ICc6Jykge1xuICAgICAgdmFyIGZpcnN0Q2hhciA9IGZpbGVwYXRoLmNoYXJDb2RlQXQoMCk7XG5cbiAgICAgIGlmIChpc1dpbmRvd3NEZXZpY2VOYW1lKGZpcnN0Q2hhcikpIHtcbiAgICAgICAgcmV0dXJuIGZpbGVwYXRoOyAvLyBpdCdzIGEgcm9vdCB3aW5kb3dzIHBhdGhcbiAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gJy4nO1xuICB9IC8vIG9ubHkgZm91bmQgcm9vdCBzZXBhcmF0b3JcblxuXG4gIGlmIChmb3VuZEluZGV4ID09PSAwKSB7XG4gICAgcmV0dXJuIHNlcGFyYXRvcjsgLy8gaWYgaXQgd2FzICcvJywgcmV0dXJuIHRoYXRcbiAgfSAvLyBIYW5kbGUgc3BlY2lhbCBjYXNlIG9mICcvL3NvbWV0aGluZydcblxuXG4gIGlmIChmb3VuZEluZGV4ID09PSAxICYmIHNlcGFyYXRvciA9PT0gJy8nICYmIGZpbGVwYXRoLmNoYXJBdCgwKSA9PT0gJy8nKSB7XG4gICAgcmV0dXJuICcvLyc7XG4gIH1cblxuICByZXR1cm4gZmlsZXBhdGguc2xpY2UoMCwgZm91bmRJbmRleCk7XG59XG4vKipcbiAqIFtleHRuYW1lIGRlc2NyaXB0aW9uXVxuICogQHBhcmFtICB7c3RyaW5nfSBzZXBhcmF0b3IgIHBsYXRmb3JtLXNwZWNpZmljIGZpbGUgc2VwYXJhdG9yXG4gKiBAcGFyYW0gIHtzdHJpbmd9IGZpbGVwYXRoICAgaW5wdXQgZmlsZSBwYXRoXG4gKiBAcmV0dXJuIHtzdHJpbmd9ICAgICAgICAgICAgW2Rlc2NyaXB0aW9uXVxuICovXG5cblxuZnVuY3Rpb24gX2V4dG5hbWUoc2VwYXJhdG9yLCBmaWxlcGF0aCkge1xuICBhc3NlcnRUeXBlKGZpbGVwYXRoLCAncGF0aCcsICdzdHJpbmcnKTtcbiAgdmFyIGluZGV4ID0gZmlsZXBhdGgubGFzdEluZGV4T2YoJy4nKTtcblxuICBpZiAoaW5kZXggPT09IC0xIHx8IGluZGV4ID09PSAwKSB7XG4gICAgcmV0dXJuICcnO1xuICB9IC8vIGlnbm9yZSB0cmFpbGluZyBzZXBhcmF0b3JcblxuXG4gIHZhciBlbmRJbmRleCA9IGZpbGVwYXRoLmxlbmd0aDtcblxuICBpZiAoZmlsZXBhdGguZW5kc1dpdGgoc2VwYXJhdG9yKSkge1xuICAgIGVuZEluZGV4LS07XG4gIH1cblxuICByZXR1cm4gZmlsZXBhdGguc2xpY2UoaW5kZXgsIGVuZEluZGV4KTtcbn1cblxuZnVuY3Rpb24gbGFzdEluZGV4V2luMzJTZXBhcmF0b3IoZmlsZXBhdGgsIGluZGV4KSB7XG4gIGZvciAodmFyIGkgPSBpbmRleDsgaSA+PSAwOyBpLS0pIHtcbiAgICB2YXIgY2hhciA9IGZpbGVwYXRoLmNoYXJDb2RlQXQoaSk7XG5cbiAgICBpZiAoY2hhciA9PT0gQkFDS1dBUkRfU0xBU0ggfHwgY2hhciA9PT0gRk9SV0FSRF9TTEFTSCkge1xuICAgICAgcmV0dXJuIGk7XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIC0xO1xufVxuLyoqXG4gKiBbYmFzZW5hbWUgZGVzY3JpcHRpb25dXG4gKiBAcGFyYW0gIHtzdHJpbmd9IHNlcGFyYXRvciAgcGxhdGZvcm0tc3BlY2lmaWMgZmlsZSBzZXBhcmF0b3JcbiAqIEBwYXJhbSAge3N0cmluZ30gZmlsZXBhdGggICBpbnB1dCBmaWxlIHBhdGhcbiAqIEBwYXJhbSAge3N0cmluZ30gW2V4dF0gICAgICBmaWxlIGV4dGVuc2lvbiB0byBkcm9wIGlmIGl0IGV4aXN0c1xuICogQHJldHVybiB7c3RyaW5nfSAgICAgICAgICAgIFtkZXNjcmlwdGlvbl1cbiAqL1xuXG5cbmZ1bmN0aW9uIF9iYXNlbmFtZShzZXBhcmF0b3IsIGZpbGVwYXRoLCBleHQpIHtcbiAgYXNzZXJ0VHlwZShmaWxlcGF0aCwgJ3BhdGgnLCAnc3RyaW5nJyk7XG5cbiAgaWYgKGV4dCAhPT0gdW5kZWZpbmVkKSB7XG4gICAgYXNzZXJ0VHlwZShleHQsICdleHQnLCAnc3RyaW5nJyk7XG4gIH1cblxuICB2YXIgbGVuZ3RoID0gZmlsZXBhdGgubGVuZ3RoO1xuXG4gIGlmIChsZW5ndGggPT09IDApIHtcbiAgICByZXR1cm4gJyc7XG4gIH1cblxuICB2YXIgaXNQb3NpeCA9IHNlcGFyYXRvciA9PT0gJy8nO1xuICB2YXIgZW5kSW5kZXggPSBsZW5ndGg7IC8vIGRyb3AgdHJhaWxpbmcgc2VwYXJhdG9yIChpZiB0aGVyZSBpcyBvbmUpXG5cbiAgdmFyIGxhc3RDaGFyQ29kZSA9IGZpbGVwYXRoLmNoYXJDb2RlQXQobGVuZ3RoIC0gMSk7XG5cbiAgaWYgKGxhc3RDaGFyQ29kZSA9PT0gRk9SV0FSRF9TTEFTSCB8fCAhaXNQb3NpeCAmJiBsYXN0Q2hhckNvZGUgPT09IEJBQ0tXQVJEX1NMQVNIKSB7XG4gICAgZW5kSW5kZXgtLTtcbiAgfSAvLyBGaW5kIGxhc3Qgb2NjdXJlbmNlIG9mIHNlcGFyYXRvclxuXG5cbiAgdmFyIGxhc3RJbmRleCA9IC0xO1xuXG4gIGlmIChpc1Bvc2l4KSB7XG4gICAgbGFzdEluZGV4ID0gZmlsZXBhdGgubGFzdEluZGV4T2Yoc2VwYXJhdG9yLCBlbmRJbmRleCAtIDEpO1xuICB9IGVsc2Uge1xuICAgIC8vIE9uIHdpbjMyLCBoYW5kbGUgKmVpdGhlciogc2VwYXJhdG9yIVxuICAgIGxhc3RJbmRleCA9IGxhc3RJbmRleFdpbjMyU2VwYXJhdG9yKGZpbGVwYXRoLCBlbmRJbmRleCAtIDEpOyAvLyBoYW5kbGUgc3BlY2lhbCBjYXNlIG9mIHJvb3QgcGF0aCBsaWtlICdDOicgb3IgJ0M6XFxcXCdcblxuICAgIGlmICgobGFzdEluZGV4ID09PSAyIHx8IGxhc3RJbmRleCA9PT0gLTEpICYmIGZpbGVwYXRoLmNoYXJBdCgxKSA9PT0gJzonICYmIGlzV2luZG93c0RldmljZU5hbWUoZmlsZXBhdGguY2hhckNvZGVBdCgwKSkpIHtcbiAgICAgIHJldHVybiAnJztcbiAgICB9XG4gIH0gLy8gVGFrZSBmcm9tIGxhc3Qgb2NjdXJyZW5jZSBvZiBzZXBhcmF0b3IgdG8gZW5kIG9mIHN0cmluZyAob3IgYmVnaW5uaW5nIHRvIGVuZCBpZiBub3QgZm91bmQpXG5cblxuICB2YXIgYmFzZSA9IGZpbGVwYXRoLnNsaWNlKGxhc3RJbmRleCArIDEsIGVuZEluZGV4KTsgLy8gZHJvcCB0cmFpbGluZyBleHRlbnNpb24gKGlmIHNwZWNpZmllZClcblxuICBpZiAoZXh0ID09PSB1bmRlZmluZWQpIHtcbiAgICByZXR1cm4gYmFzZTtcbiAgfVxuXG4gIHJldHVybiBiYXNlLmVuZHNXaXRoKGV4dCkgPyBiYXNlLnNsaWNlKDAsIGJhc2UubGVuZ3RoIC0gZXh0Lmxlbmd0aCkgOiBiYXNlO1xufVxuLyoqXG4gKiBUaGUgYHBhdGgubm9ybWFsaXplKClgIG1ldGhvZCBub3JtYWxpemVzIHRoZSBnaXZlbiBwYXRoLCByZXNvbHZpbmcgJy4uJyBhbmQgJy4nIHNlZ21lbnRzLlxuICpcbiAqIFdoZW4gbXVsdGlwbGUsIHNlcXVlbnRpYWwgcGF0aCBzZWdtZW50IHNlcGFyYXRpb24gY2hhcmFjdGVycyBhcmUgZm91bmQgKGUuZy5cbiAqIC8gb24gUE9TSVggYW5kIGVpdGhlciBcXCBvciAvIG9uIFdpbmRvd3MpLCB0aGV5IGFyZSByZXBsYWNlZCBieSBhIHNpbmdsZVxuICogaW5zdGFuY2Ugb2YgdGhlIHBsYXRmb3JtLXNwZWNpZmljIHBhdGggc2VnbWVudCBzZXBhcmF0b3IgKC8gb24gUE9TSVggYW5kIFxcXG4gKiBvbiBXaW5kb3dzKS4gVHJhaWxpbmcgc2VwYXJhdG9ycyBhcmUgcHJlc2VydmVkLlxuICpcbiAqIElmIHRoZSBwYXRoIGlzIGEgemVyby1sZW5ndGggc3RyaW5nLCAnLicgaXMgcmV0dXJuZWQsIHJlcHJlc2VudGluZyB0aGVcbiAqIGN1cnJlbnQgd29ya2luZyBkaXJlY3RvcnkuXG4gKlxuICogQHBhcmFtICB7c3RyaW5nfSBzZXBhcmF0b3IgIHBsYXRmb3JtLXNwZWNpZmljIGZpbGUgc2VwYXJhdG9yXG4gKiBAcGFyYW0gIHtzdHJpbmd9IGZpbGVwYXRoICBpbnB1dCBmaWxlIHBhdGhcbiAqIEByZXR1cm4ge3N0cmluZ30gW2Rlc2NyaXB0aW9uXVxuICovXG5cblxuZnVuY3Rpb24gX25vcm1hbGl6ZShzZXBhcmF0b3IsIGZpbGVwYXRoKSB7XG4gIGFzc2VydFR5cGUoZmlsZXBhdGgsICdwYXRoJywgJ3N0cmluZycpO1xuXG4gIGlmIChmaWxlcGF0aC5sZW5ndGggPT09IDApIHtcbiAgICByZXR1cm4gJy4nO1xuICB9IC8vIFdpbmRvd3MgY2FuIGhhbmRsZSAnLycgb3IgJ1xcXFwnIGFuZCBib3RoIHNob3VsZCBiZSB0dXJuZWQgaW50byBzZXBhcmF0b3JcblxuXG4gIHZhciBpc1dpbmRvd3MgPSBzZXBhcmF0b3IgPT09ICdcXFxcJztcblxuICBpZiAoaXNXaW5kb3dzKSB7XG4gICAgZmlsZXBhdGggPSBmaWxlcGF0aC5yZXBsYWNlKC9cXC8vZywgc2VwYXJhdG9yKTtcbiAgfVxuXG4gIHZhciBoYWRMZWFkaW5nID0gZmlsZXBhdGguc3RhcnRzV2l0aChzZXBhcmF0b3IpOyAvLyBPbiBXaW5kb3dzLCBuZWVkIHRvIGhhbmRsZSBVTkMgcGF0aHMgKFxcXFxob3N0LW5hbWVcXFxccmVzb3VyY2VcXFxcZGlyKSBzcGVjaWFsIHRvIHJldGFpbiBsZWFkaW5nIGRvdWJsZSBiYWNrc2xhc2hcblxuICB2YXIgaXNVTkMgPSBoYWRMZWFkaW5nICYmIGlzV2luZG93cyAmJiBmaWxlcGF0aC5sZW5ndGggPiAyICYmIGZpbGVwYXRoLmNoYXJBdCgxKSA9PT0gJ1xcXFwnO1xuICB2YXIgaGFkVHJhaWxpbmcgPSBmaWxlcGF0aC5lbmRzV2l0aChzZXBhcmF0b3IpO1xuICB2YXIgcGFydHMgPSBmaWxlcGF0aC5zcGxpdChzZXBhcmF0b3IpO1xuICB2YXIgcmVzdWx0ID0gW107XG4gIHZhciBfaXRlcmF0b3JOb3JtYWxDb21wbGV0aW9uID0gdHJ1ZTtcbiAgdmFyIF9kaWRJdGVyYXRvckVycm9yID0gZmFsc2U7XG4gIHZhciBfaXRlcmF0b3JFcnJvciA9IHVuZGVmaW5lZDtcblxuICB0cnkge1xuICAgIGZvciAodmFyIF9pdGVyYXRvciA9IHBhcnRzW1N5bWJvbC5pdGVyYXRvcl0oKSwgX3N0ZXA7ICEoX2l0ZXJhdG9yTm9ybWFsQ29tcGxldGlvbiA9IChfc3RlcCA9IF9pdGVyYXRvci5uZXh0KCkpLmRvbmUpOyBfaXRlcmF0b3JOb3JtYWxDb21wbGV0aW9uID0gdHJ1ZSkge1xuICAgICAgdmFyIHNlZ21lbnQgPSBfc3RlcC52YWx1ZTtcblxuICAgICAgaWYgKHNlZ21lbnQubGVuZ3RoICE9PSAwICYmIHNlZ21lbnQgIT09ICcuJykge1xuICAgICAgICBpZiAoc2VnbWVudCA9PT0gJy4uJykge1xuICAgICAgICAgIHJlc3VsdC5wb3AoKTsgLy8gRklYTUU6IFdoYXQgaWYgdGhpcyBnb2VzIGFib3ZlIHJvb3Q/IFNob3VsZCB3ZSB0aHJvdyBhbiBlcnJvcj9cbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICByZXN1bHQucHVzaChzZWdtZW50KTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfSBjYXRjaCAoZXJyKSB7XG4gICAgX2RpZEl0ZXJhdG9yRXJyb3IgPSB0cnVlO1xuICAgIF9pdGVyYXRvckVycm9yID0gZXJyO1xuICB9IGZpbmFsbHkge1xuICAgIHRyeSB7XG4gICAgICBpZiAoIV9pdGVyYXRvck5vcm1hbENvbXBsZXRpb24gJiYgX2l0ZXJhdG9yLnJldHVybiAhPSBudWxsKSB7XG4gICAgICAgIF9pdGVyYXRvci5yZXR1cm4oKTtcbiAgICAgIH1cbiAgICB9IGZpbmFsbHkge1xuICAgICAgaWYgKF9kaWRJdGVyYXRvckVycm9yKSB7XG4gICAgICAgIHRocm93IF9pdGVyYXRvckVycm9yO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIHZhciBub3JtYWxpemVkID0gaGFkTGVhZGluZyA/IHNlcGFyYXRvciA6ICcnO1xuICBub3JtYWxpemVkICs9IHJlc3VsdC5qb2luKHNlcGFyYXRvcik7XG5cbiAgaWYgKGhhZFRyYWlsaW5nKSB7XG4gICAgbm9ybWFsaXplZCArPSBzZXBhcmF0b3I7XG4gIH1cblxuICBpZiAoaXNVTkMpIHtcbiAgICBub3JtYWxpemVkID0gJ1xcXFwnICsgbm9ybWFsaXplZDtcbiAgfVxuXG4gIHJldHVybiBub3JtYWxpemVkO1xufVxuLyoqXG4gKiBbYXNzZXJ0U2VnbWVudCBkZXNjcmlwdGlvbl1cbiAqIEBwYXJhbSAgeyp9IHNlZ21lbnQgW2Rlc2NyaXB0aW9uXVxuICogQHJldHVybiB7dm9pZH0gICAgICAgICBbZGVzY3JpcHRpb25dXG4gKi9cblxuXG5mdW5jdGlvbiBhc3NlcnRTZWdtZW50KHNlZ21lbnQpIHtcbiAgaWYgKHR5cGVvZiBzZWdtZW50ICE9PSAnc3RyaW5nJykge1xuICAgIHRocm93IG5ldyBUeXBlRXJyb3IoYFBhdGggbXVzdCBiZSBhIHN0cmluZy4gUmVjZWl2ZWQgJHtzZWdtZW50fWApO1xuICB9XG59XG4vKipcbiAqIFRoZSBgcGF0aC5qb2luKClgIG1ldGhvZCBqb2lucyBhbGwgZ2l2ZW4gcGF0aCBzZWdtZW50cyB0b2dldGhlciB1c2luZyB0aGVcbiAqIHBsYXRmb3JtLXNwZWNpZmljIHNlcGFyYXRvciBhcyBhIGRlbGltaXRlciwgdGhlbiBub3JtYWxpemVzIHRoZSByZXN1bHRpbmcgcGF0aC5cbiAqIFplcm8tbGVuZ3RoIHBhdGggc2VnbWVudHMgYXJlIGlnbm9yZWQuIElmIHRoZSBqb2luZWQgcGF0aCBzdHJpbmcgaXMgYSB6ZXJvLVxuICogbGVuZ3RoIHN0cmluZyB0aGVuICcuJyB3aWxsIGJlIHJldHVybmVkLCByZXByZXNlbnRpbmcgdGhlIGN1cnJlbnQgd29ya2luZyBkaXJlY3RvcnkuXG4gKiBAcGFyYW0gIHtzdHJpbmd9IHNlcGFyYXRvciBwbGF0Zm9ybS1zcGVjaWZpYyBmaWxlIHNlcGFyYXRvclxuICogQHBhcmFtICB7c3RyaW5nW119IHBhdGhzIFtkZXNjcmlwdGlvbl1cbiAqIEByZXR1cm4ge3N0cmluZ30gICAgICAgVGhlIGpvaW5lZCBmaWxlcGF0aFxuICovXG5cblxuZnVuY3Rpb24gX2pvaW4oc2VwYXJhdG9yLCBwYXRocykge1xuICB2YXIgcmVzdWx0ID0gW107IC8vIG5haXZlIGltcGw6IGp1c3Qgam9pbiBhbGwgdGhlIHBhdGhzIHdpdGggc2VwYXJhdG9yXG5cbiAgdmFyIF9pdGVyYXRvck5vcm1hbENvbXBsZXRpb24yID0gdHJ1ZTtcbiAgdmFyIF9kaWRJdGVyYXRvckVycm9yMiA9IGZhbHNlO1xuICB2YXIgX2l0ZXJhdG9yRXJyb3IyID0gdW5kZWZpbmVkO1xuXG4gIHRyeSB7XG4gICAgZm9yICh2YXIgX2l0ZXJhdG9yMiA9IHBhdGhzW1N5bWJvbC5pdGVyYXRvcl0oKSwgX3N0ZXAyOyAhKF9pdGVyYXRvck5vcm1hbENvbXBsZXRpb24yID0gKF9zdGVwMiA9IF9pdGVyYXRvcjIubmV4dCgpKS5kb25lKTsgX2l0ZXJhdG9yTm9ybWFsQ29tcGxldGlvbjIgPSB0cnVlKSB7XG4gICAgICB2YXIgc2VnbWVudCA9IF9zdGVwMi52YWx1ZTtcbiAgICAgIGFzc2VydFNlZ21lbnQoc2VnbWVudCk7XG5cbiAgICAgIGlmIChzZWdtZW50Lmxlbmd0aCAhPT0gMCkge1xuICAgICAgICByZXN1bHQucHVzaChzZWdtZW50KTtcbiAgICAgIH1cbiAgICB9XG4gIH0gY2F0Y2ggKGVycikge1xuICAgIF9kaWRJdGVyYXRvckVycm9yMiA9IHRydWU7XG4gICAgX2l0ZXJhdG9yRXJyb3IyID0gZXJyO1xuICB9IGZpbmFsbHkge1xuICAgIHRyeSB7XG4gICAgICBpZiAoIV9pdGVyYXRvck5vcm1hbENvbXBsZXRpb24yICYmIF9pdGVyYXRvcjIucmV0dXJuICE9IG51bGwpIHtcbiAgICAgICAgX2l0ZXJhdG9yMi5yZXR1cm4oKTtcbiAgICAgIH1cbiAgICB9IGZpbmFsbHkge1xuICAgICAgaWYgKF9kaWRJdGVyYXRvckVycm9yMikge1xuICAgICAgICB0aHJvdyBfaXRlcmF0b3JFcnJvcjI7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIF9ub3JtYWxpemUoc2VwYXJhdG9yLCByZXN1bHQuam9pbihzZXBhcmF0b3IpKTtcbn1cbi8qKlxuICogVGhlIGBwYXRoLnJlc29sdmUoKWAgbWV0aG9kIHJlc29sdmVzIGEgc2VxdWVuY2Ugb2YgcGF0aHMgb3IgcGF0aCBzZWdtZW50cyBpbnRvIGFuIGFic29sdXRlIHBhdGguXG4gKlxuICogQHBhcmFtICB7c3RyaW5nfSBzZXBhcmF0b3IgcGxhdGZvcm0tc3BlY2lmaWMgZmlsZSBzZXBhcmF0b3JcbiAqIEBwYXJhbSAge3N0cmluZ1tdfSBwYXRocyBbZGVzY3JpcHRpb25dXG4gKiBAcmV0dXJuIHtzdHJpbmd9ICAgICAgIFtkZXNjcmlwdGlvbl1cbiAqL1xuXG5cbmZ1bmN0aW9uIF9yZXNvbHZlKHNlcGFyYXRvciwgcGF0aHMpIHtcbiAgdmFyIHJlc29sdmVkID0gJyc7XG4gIHZhciBoaXRSb290ID0gZmFsc2U7XG4gIHZhciBpc1Bvc2l4ID0gc2VwYXJhdG9yID09PSAnLyc7IC8vIGdvIGZyb20gcmlnaHQgdG8gbGVmdCB1bnRpbCB3ZSBoaXQgYWJzb2x1dGUgcGF0aC9yb290XG5cbiAgZm9yICh2YXIgaSA9IHBhdGhzLmxlbmd0aCAtIDE7IGkgPj0gMDsgaS0tKSB7XG4gICAgdmFyIHNlZ21lbnQgPSBwYXRoc1tpXTtcbiAgICBhc3NlcnRTZWdtZW50KHNlZ21lbnQpO1xuXG4gICAgaWYgKHNlZ21lbnQubGVuZ3RoID09PSAwKSB7XG4gICAgICBjb250aW51ZTsgLy8gc2tpcCBlbXB0eVxuICAgIH1cblxuICAgIHJlc29sdmVkID0gc2VnbWVudCArIHNlcGFyYXRvciArIHJlc29sdmVkOyAvLyBwcmVwZW5kIG5ldyBzZWdtZW50XG5cbiAgICBpZiAoX2lzQWJzb2x1dGUoaXNQb3NpeCwgc2VnbWVudCkpIHtcbiAgICAgIC8vIGhhdmUgd2UgYmFja2VkIGludG8gYW4gYWJzb2x1dGUgcGF0aD9cbiAgICAgIGhpdFJvb3QgPSB0cnVlO1xuICAgICAgYnJlYWs7XG4gICAgfVxuICB9IC8vIGlmIHdlIGRpZG4ndCBoaXQgcm9vdCwgcHJlcGVuZCBjd2RcblxuXG4gIGlmICghaGl0Um9vdCkge1xuICAgIHJlc29sdmVkID0gcHJvY2Vzcy5jd2QoKSArIHNlcGFyYXRvciArIHJlc29sdmVkO1xuICB9XG5cbiAgdmFyIG5vcm1hbGl6ZWQgPSBfbm9ybWFsaXplKHNlcGFyYXRvciwgcmVzb2x2ZWQpO1xuXG4gIGlmIChub3JtYWxpemVkLmNoYXJBdChub3JtYWxpemVkLmxlbmd0aCAtIDEpID09PSBzZXBhcmF0b3IpIHtcbiAgICAvLyBGSVhNRTogSGFuZGxlIFVOQyBwYXRocyBvbiBXaW5kb3dzIGFzIHdlbGwsIHNvIHdlIGRvbid0IHRyaW0gdHJhaWxpbmcgc2VwYXJhdG9yIG9uIHNvbWV0aGluZyBsaWtlICdcXFxcXFxcXGhvc3QtbmFtZVxcXFxyZXNvdXJjZVxcXFwnXG4gICAgLy8gRG9uJ3QgcmVtb3ZlIHRyYWlsaW5nIHNlcGFyYXRvciBpZiB0aGlzIGlzIHJvb3QgcGF0aCBvbiB3aW5kb3dzIVxuICAgIGlmICghaXNQb3NpeCAmJiBub3JtYWxpemVkLmxlbmd0aCA9PT0gMyAmJiBub3JtYWxpemVkLmNoYXJBdCgxKSA9PT0gJzonICYmIGlzV2luZG93c0RldmljZU5hbWUobm9ybWFsaXplZC5jaGFyQ29kZUF0KDApKSkge1xuICAgICAgcmV0dXJuIG5vcm1hbGl6ZWQ7XG4gICAgfSAvLyBvdGhlcndpc2UgdHJpbSB0cmFpbGluZyBzZXBhcmF0b3JcblxuXG4gICAgcmV0dXJuIG5vcm1hbGl6ZWQuc2xpY2UoMCwgbm9ybWFsaXplZC5sZW5ndGggLSAxKTtcbiAgfVxuXG4gIHJldHVybiBub3JtYWxpemVkO1xufVxuLyoqXG4gKiBUaGUgYHBhdGgucmVsYXRpdmUoKWAgbWV0aG9kIHJldHVybnMgdGhlIHJlbGF0aXZlIHBhdGggYGZyb21gIGZyb20gdG8gYHRvYCBiYXNlZFxuICogb24gdGhlIGN1cnJlbnQgd29ya2luZyBkaXJlY3RvcnkuIElmIGZyb20gYW5kIHRvIGVhY2ggcmVzb2x2ZSB0byB0aGUgc2FtZVxuICogcGF0aCAoYWZ0ZXIgY2FsbGluZyBgcGF0aC5yZXNvbHZlKClgIG9uIGVhY2gpLCBhIHplcm8tbGVuZ3RoIHN0cmluZyBpcyByZXR1cm5lZC5cbiAqXG4gKiBJZiBhIHplcm8tbGVuZ3RoIHN0cmluZyBpcyBwYXNzZWQgYXMgYGZyb21gIG9yIGB0b2AsIHRoZSBjdXJyZW50IHdvcmtpbmcgZGlyZWN0b3J5XG4gKiB3aWxsIGJlIHVzZWQgaW5zdGVhZCBvZiB0aGUgemVyby1sZW5ndGggc3RyaW5ncy5cbiAqXG4gKiBAcGFyYW0gIHtzdHJpbmd9IHNlcGFyYXRvciBwbGF0Zm9ybS1zcGVjaWZpYyBmaWxlIHNlcGFyYXRvclxuICogQHBhcmFtICB7c3RyaW5nfSBmcm9tIFtkZXNjcmlwdGlvbl1cbiAqIEBwYXJhbSAge3N0cmluZ30gdG8gICBbZGVzY3JpcHRpb25dXG4gKiBAcmV0dXJuIHtzdHJpbmd9ICAgICAgW2Rlc2NyaXB0aW9uXVxuICovXG5cblxuZnVuY3Rpb24gX3JlbGF0aXZlKHNlcGFyYXRvciwgZnJvbSwgdG8pIHtcbiAgYXNzZXJ0VHlwZShmcm9tLCAnZnJvbScsICdzdHJpbmcnKTtcbiAgYXNzZXJ0VHlwZSh0bywgJ3RvJywgJ3N0cmluZycpO1xuXG4gIGlmIChmcm9tID09PSB0bykge1xuICAgIHJldHVybiAnJztcbiAgfVxuXG4gIGZyb20gPSBfcmVzb2x2ZShzZXBhcmF0b3IsIFtmcm9tXSk7XG4gIHRvID0gX3Jlc29sdmUoc2VwYXJhdG9yLCBbdG9dKTtcblxuICBpZiAoZnJvbSA9PT0gdG8pIHtcbiAgICByZXR1cm4gJyc7XG4gIH0gLy8gd2Ugbm93IGhhdmUgdHdvIGFic29sdXRlIHBhdGhzLFxuICAvLyBsZXRzIFwiZ28gdXBcIiBmcm9tIGBmcm9tYCB1bnRpbCB3ZSByZWFjaCBjb21tb24gYmFzZSBkaXIgb2YgYHRvYFxuICAvLyBjb25zdCBvcmlnaW5hbEZyb20gPSBmcm9tO1xuXG5cbiAgdmFyIHVwQ291bnQgPSAwO1xuICB2YXIgcmVtYWluaW5nUGF0aCA9ICcnO1xuXG4gIHdoaWxlICh0cnVlKSB7XG4gICAgaWYgKHRvLnN0YXJ0c1dpdGgoZnJvbSkpIHtcbiAgICAgIC8vIG1hdGNoISByZWNvcmQgcmVzdC4uLj9cbiAgICAgIHJlbWFpbmluZ1BhdGggPSB0by5zbGljZShmcm9tLmxlbmd0aCk7XG4gICAgICBicmVhaztcbiAgICB9IC8vIEZJWE1FOiBCcmVhay90aHJvdyBpZiB3ZSBoaXQgYmFkIGVkZ2UgY2FzZSBvZiBubyBjb21tb24gcm9vdCFcblxuXG4gICAgZnJvbSA9IF9kaXJuYW1lKHNlcGFyYXRvciwgZnJvbSk7XG4gICAgdXBDb3VudCsrO1xuICB9IC8vIHJlbW92ZSBsZWFkaW5nIHNlcGFyYXRvciBmcm9tIHJlbWFpbmluZ1BhdGggaWYgdGhlcmUgaXMgYW55XG5cblxuICBpZiAocmVtYWluaW5nUGF0aC5sZW5ndGggPiAwKSB7XG4gICAgcmVtYWluaW5nUGF0aCA9IHJlbWFpbmluZ1BhdGguc2xpY2UoMSk7XG4gIH1cblxuICByZXR1cm4gKCcuLicgKyBzZXBhcmF0b3IpLnJlcGVhdCh1cENvdW50KSArIHJlbWFpbmluZ1BhdGg7XG59XG4vKipcbiAqIFRoZSBgcGF0aC5wYXJzZSgpYCBtZXRob2QgcmV0dXJucyBhbiBvYmplY3Qgd2hvc2UgcHJvcGVydGllcyByZXByZXNlbnRcbiAqIHNpZ25pZmljYW50IGVsZW1lbnRzIG9mIHRoZSBwYXRoLiBUcmFpbGluZyBkaXJlY3Rvcnkgc2VwYXJhdG9ycyBhcmUgaWdub3JlZCxcbiAqIHNlZSBgcGF0aC5zZXBgLlxuICpcbiAqIFRoZSByZXR1cm5lZCBvYmplY3Qgd2lsbCBoYXZlIHRoZSBmb2xsb3dpbmcgcHJvcGVydGllczpcbiAqXG4gKiAtIGRpciA8c3RyaW5nPlxuICogLSByb290IDxzdHJpbmc+XG4gKiAtIGJhc2UgPHN0cmluZz5cbiAqIC0gbmFtZSA8c3RyaW5nPlxuICogLSBleHQgPHN0cmluZz5cbiAqIEBwYXJhbSAge3N0cmluZ30gc2VwYXJhdG9yIHBsYXRmb3JtLXNwZWNpZmljIGZpbGUgc2VwYXJhdG9yXG4gKiBAcGFyYW0gIHtzdHJpbmd9IGZpbGVwYXRoIFtkZXNjcmlwdGlvbl1cbiAqIEByZXR1cm4ge29iamVjdH1cbiAqL1xuXG5cbmZ1bmN0aW9uIF9wYXJzZShzZXBhcmF0b3IsIGZpbGVwYXRoKSB7XG4gIGFzc2VydFR5cGUoZmlsZXBhdGgsICdwYXRoJywgJ3N0cmluZycpO1xuICB2YXIgcmVzdWx0ID0ge1xuICAgIHJvb3Q6ICcnLFxuICAgIGRpcjogJycsXG4gICAgYmFzZTogJycsXG4gICAgZXh0OiAnJyxcbiAgICBuYW1lOiAnJ1xuICB9O1xuICB2YXIgbGVuZ3RoID0gZmlsZXBhdGgubGVuZ3RoO1xuXG4gIGlmIChsZW5ndGggPT09IDApIHtcbiAgICByZXR1cm4gcmVzdWx0O1xuICB9IC8vIENoZWF0IGFuZCBqdXN0IGNhbGwgb3VyIG90aGVyIG1ldGhvZHMgZm9yIGRpcm5hbWUvYmFzZW5hbWUvZXh0bmFtZT9cblxuXG4gIHJlc3VsdC5iYXNlID0gX2Jhc2VuYW1lKHNlcGFyYXRvciwgZmlsZXBhdGgpO1xuICByZXN1bHQuZXh0ID0gX2V4dG5hbWUoc2VwYXJhdG9yLCByZXN1bHQuYmFzZSk7XG4gIHZhciBiYXNlTGVuZ3RoID0gcmVzdWx0LmJhc2UubGVuZ3RoO1xuICByZXN1bHQubmFtZSA9IHJlc3VsdC5iYXNlLnNsaWNlKDAsIGJhc2VMZW5ndGggLSByZXN1bHQuZXh0Lmxlbmd0aCk7XG4gIHZhciB0b1N1YnRyYWN0ID0gYmFzZUxlbmd0aCA9PT0gMCA/IDAgOiBiYXNlTGVuZ3RoICsgMTtcbiAgcmVzdWx0LmRpciA9IGZpbGVwYXRoLnNsaWNlKDAsIGZpbGVwYXRoLmxlbmd0aCAtIHRvU3VidHJhY3QpOyAvLyBkcm9wIHRyYWlsaW5nIHNlcGFyYXRvciFcblxuICB2YXIgZmlyc3RDaGFyQ29kZSA9IGZpbGVwYXRoLmNoYXJDb2RlQXQoMCk7IC8vIGJvdGggd2luMzIgYW5kIFBPU0lYIHJldHVybiAnLycgcm9vdFxuXG4gIGlmIChmaXJzdENoYXJDb2RlID09PSBGT1JXQVJEX1NMQVNIKSB7XG4gICAgcmVzdWx0LnJvb3QgPSAnLyc7XG4gICAgcmV0dXJuIHJlc3VsdDtcbiAgfSAvLyB3ZSdyZSBkb25lIHdpdGggUE9TSVguLi5cblxuXG4gIGlmIChzZXBhcmF0b3IgPT09ICcvJykge1xuICAgIHJldHVybiByZXN1bHQ7XG4gIH0gLy8gZm9yIHdpbjMyLi4uXG5cblxuICBpZiAoZmlyc3RDaGFyQ29kZSA9PT0gQkFDS1dBUkRfU0xBU0gpIHtcbiAgICAvLyBGSVhNRTogSGFuZGxlIFVOQyBwYXRocyBsaWtlICdcXFxcXFxcXGhvc3QtbmFtZVxcXFxyZXNvdXJjZVxcXFxmaWxlX3BhdGgnXG4gICAgLy8gbmVlZCB0byByZXRhaW4gJ1xcXFxcXFxcaG9zdC1uYW1lXFxcXHJlc291cmNlXFxcXCcgYXMgcm9vdCBpbiB0aGF0IGNhc2UhXG4gICAgcmVzdWx0LnJvb3QgPSAnXFxcXCc7XG4gICAgcmV0dXJuIHJlc3VsdDtcbiAgfSAvLyBjaGVjayBmb3IgQzogc3R5bGUgcm9vdFxuXG5cbiAgaWYgKGxlbmd0aCA+IDEgJiYgaXNXaW5kb3dzRGV2aWNlTmFtZShmaXJzdENoYXJDb2RlKSAmJiBmaWxlcGF0aC5jaGFyQXQoMSkgPT09ICc6Jykge1xuICAgIGlmIChsZW5ndGggPiAyKSB7XG4gICAgICAvLyBpcyBpdCBsaWtlIEM6XFxcXD9cbiAgICAgIHZhciB0aGlyZENoYXJDb2RlID0gZmlsZXBhdGguY2hhckNvZGVBdCgyKTtcblxuICAgICAgaWYgKHRoaXJkQ2hhckNvZGUgPT09IEZPUldBUkRfU0xBU0ggfHwgdGhpcmRDaGFyQ29kZSA9PT0gQkFDS1dBUkRfU0xBU0gpIHtcbiAgICAgICAgcmVzdWx0LnJvb3QgPSBmaWxlcGF0aC5zbGljZSgwLCAzKTtcbiAgICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICAgIH1cbiAgICB9IC8vIG5vcGUsIGp1c3QgQzosIG5vIHRyYWlsaW5nIHNlcGFyYXRvclxuXG5cbiAgICByZXN1bHQucm9vdCA9IGZpbGVwYXRoLnNsaWNlKDAsIDIpO1xuICB9XG5cbiAgcmV0dXJuIHJlc3VsdDtcbn1cbi8qKlxuICogVGhlIGBwYXRoLmZvcm1hdCgpYCBtZXRob2QgcmV0dXJucyBhIHBhdGggc3RyaW5nIGZyb20gYW4gb2JqZWN0LiBUaGlzIGlzIHRoZVxuICogb3Bwb3NpdGUgb2YgYHBhdGgucGFyc2UoKWAuXG4gKlxuICogQHBhcmFtICB7c3RyaW5nfSBzZXBhcmF0b3IgcGxhdGZvcm0tc3BlY2lmaWMgZmlsZSBzZXBhcmF0b3JcbiAqIEBwYXJhbSAge29iamVjdH0gcGF0aE9iamVjdCBvYmplY3Qgb2YgZm9ybWF0IHJldHVybmVkIGJ5IGBwYXRoLnBhcnNlKClgXG4gKiBAcGFyYW0gIHtzdHJpbmd9IHBhdGhPYmplY3QuZGlyIGRpcmVjdG9yeSBuYW1lXG4gKiBAcGFyYW0gIHtzdHJpbmd9IHBhdGhPYmplY3Qucm9vdCBmaWxlIHJvb3QgZGlyLCBpZ25vcmVkIGlmIGBwYXRoT2JqZWN0LmRpcmAgaXMgcHJvdmlkZWRcbiAqIEBwYXJhbSAge3N0cmluZ30gcGF0aE9iamVjdC5iYXNlIGZpbGUgYmFzZW5hbWVcbiAqIEBwYXJhbSAge3N0cmluZ30gcGF0aE9iamVjdC5uYW1lIGJhc2VuYW1lIG1pbnVzIGV4dGVuc2lvbiwgaWdub3JlZCBpZiBgcGF0aE9iamVjdC5iYXNlYCBleGlzdHNcbiAqIEBwYXJhbSAge3N0cmluZ30gcGF0aE9iamVjdC5leHQgZmlsZSBleHRlbnNpb24sIGlnbm9yZWQgaWYgYHBhdGhPYmplY3QuYmFzZWAgZXhpc3RzXG4gKiBAcmV0dXJuIHtzdHJpbmd9XG4gKi9cblxuXG5mdW5jdGlvbiBfZm9ybWF0KHNlcGFyYXRvciwgcGF0aE9iamVjdCkge1xuICBhc3NlcnRUeXBlKHBhdGhPYmplY3QsICdwYXRoT2JqZWN0JywgJ29iamVjdCcpO1xuICB2YXIgYmFzZSA9IHBhdGhPYmplY3QuYmFzZSB8fCBgJHtwYXRoT2JqZWN0Lm5hbWUgfHwgJyd9JHtwYXRoT2JqZWN0LmV4dCB8fCAnJ31gOyAvLyBhcHBlbmQgYmFzZSB0byByb290IGlmIGBkaXJgIHdhc24ndCBzcGVjaWZpZWQsIG9yIGlmXG4gIC8vIGRpciBpcyB0aGUgcm9vdFxuXG4gIGlmICghcGF0aE9iamVjdC5kaXIgfHwgcGF0aE9iamVjdC5kaXIgPT09IHBhdGhPYmplY3Qucm9vdCkge1xuICAgIHJldHVybiBgJHtwYXRoT2JqZWN0LnJvb3QgfHwgJyd9JHtiYXNlfWA7XG4gIH0gLy8gY29tYmluZSBkaXIgKyAvICsgYmFzZVxuXG5cbiAgcmV0dXJuIGAke3BhdGhPYmplY3QuZGlyfSR7c2VwYXJhdG9yfSR7YmFzZX1gO1xufVxuLyoqXG4gKiBPbiBXaW5kb3dzIHN5c3RlbXMgb25seSwgcmV0dXJucyBhbiBlcXVpdmFsZW50IG5hbWVzcGFjZS1wcmVmaXhlZCBwYXRoIGZvclxuICogdGhlIGdpdmVuIHBhdGguIElmIHBhdGggaXMgbm90IGEgc3RyaW5nLCBwYXRoIHdpbGwgYmUgcmV0dXJuZWQgd2l0aG91dCBtb2RpZmljYXRpb25zLlxuICogU2VlIGh0dHBzOi8vZG9jcy5taWNyb3NvZnQuY29tL2VuLXVzL3dpbmRvd3MvZGVza3RvcC9GaWxlSU8vbmFtaW5nLWEtZmlsZSNuYW1lc3BhY2VzXG4gKiBAcGFyYW0gIHtzdHJpbmd9IGZpbGVwYXRoIFtkZXNjcmlwdGlvbl1cbiAqIEByZXR1cm4ge3N0cmluZ30gICAgICAgICAgW2Rlc2NyaXB0aW9uXVxuICovXG5cblxuZnVuY3Rpb24gdG9OYW1lc3BhY2VkUGF0aChmaWxlcGF0aCkge1xuICBpZiAodHlwZW9mIGZpbGVwYXRoICE9PSAnc3RyaW5nJykge1xuICAgIHJldHVybiBmaWxlcGF0aDtcbiAgfVxuXG4gIGlmIChmaWxlcGF0aC5sZW5ndGggPT09IDApIHtcbiAgICByZXR1cm4gJyc7XG4gIH1cblxuICB2YXIgcmVzb2x2ZWRQYXRoID0gX3Jlc29sdmUoJ1xcXFwnLCBbZmlsZXBhdGhdKTtcblxuICB2YXIgbGVuZ3RoID0gcmVzb2x2ZWRQYXRoLmxlbmd0aDtcblxuICBpZiAobGVuZ3RoIDwgMikge1xuICAgIC8vIG5lZWQgJ1xcXFxcXFxcJyBvciAnQzonIG1pbmltdW1cbiAgICByZXR1cm4gZmlsZXBhdGg7XG4gIH1cblxuICB2YXIgZmlyc3RDaGFyQ29kZSA9IHJlc29sdmVkUGF0aC5jaGFyQ29kZUF0KDApOyAvLyBpZiBzdGFydCB3aXRoICdcXFxcXFxcXCcsIHByZWZpeCB3aXRoIFVOQyByb290LCBkcm9wIHRoZSBzbGFzaGVzXG5cbiAgaWYgKGZpcnN0Q2hhckNvZGUgPT09IEJBQ0tXQVJEX1NMQVNIICYmIHJlc29sdmVkUGF0aC5jaGFyQXQoMSkgPT09ICdcXFxcJykge1xuICAgIC8vIHJldHVybiBhcy1pcyBpZiBpdCdzIGFuIGFyZWFkeSBsb25nIHBhdGggKCdcXFxcXFxcXD9cXFxcJyBvciAnXFxcXFxcXFwuXFxcXCcgcHJlZml4KVxuICAgIGlmIChsZW5ndGggPj0gMykge1xuICAgICAgdmFyIHRoaXJkQ2hhciA9IHJlc29sdmVkUGF0aC5jaGFyQXQoMik7XG5cbiAgICAgIGlmICh0aGlyZENoYXIgPT09ICc/JyB8fCB0aGlyZENoYXIgPT09ICcuJykge1xuICAgICAgICByZXR1cm4gZmlsZXBhdGg7XG4gICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuICdcXFxcXFxcXD9cXFxcVU5DXFxcXCcgKyByZXNvbHZlZFBhdGguc2xpY2UoMik7XG4gIH0gZWxzZSBpZiAoaXNXaW5kb3dzRGV2aWNlTmFtZShmaXJzdENoYXJDb2RlKSAmJiByZXNvbHZlZFBhdGguY2hhckF0KDEpID09PSAnOicpIHtcbiAgICByZXR1cm4gJ1xcXFxcXFxcP1xcXFwnICsgcmVzb2x2ZWRQYXRoO1xuICB9XG5cbiAgcmV0dXJuIGZpbGVwYXRoO1xufVxuXG52YXIgV2luMzJQYXRoID0ge1xuICBzZXA6ICdcXFxcJyxcbiAgZGVsaW1pdGVyOiAnOycsXG4gIGJhc2VuYW1lOiBmdW5jdGlvbiBiYXNlbmFtZShmaWxlcGF0aCwgZXh0KSB7XG4gICAgcmV0dXJuIF9iYXNlbmFtZSh0aGlzLnNlcCwgZmlsZXBhdGgsIGV4dCk7XG4gIH0sXG4gIG5vcm1hbGl6ZTogZnVuY3Rpb24gbm9ybWFsaXplKGZpbGVwYXRoKSB7XG4gICAgcmV0dXJuIF9ub3JtYWxpemUodGhpcy5zZXAsIGZpbGVwYXRoKTtcbiAgfSxcbiAgam9pbjogZnVuY3Rpb24gam9pbigpIHtcbiAgICBmb3IgKHZhciBfbGVuID0gYXJndW1lbnRzLmxlbmd0aCwgcGF0aHMgPSBuZXcgQXJyYXkoX2xlbiksIF9rZXkgPSAwOyBfa2V5IDwgX2xlbjsgX2tleSsrKSB7XG4gICAgICBwYXRoc1tfa2V5XSA9IGFyZ3VtZW50c1tfa2V5XTtcbiAgICB9XG5cbiAgICByZXR1cm4gX2pvaW4odGhpcy5zZXAsIHBhdGhzKTtcbiAgfSxcbiAgZXh0bmFtZTogZnVuY3Rpb24gZXh0bmFtZShmaWxlcGF0aCkge1xuICAgIHJldHVybiBfZXh0bmFtZSh0aGlzLnNlcCwgZmlsZXBhdGgpO1xuICB9LFxuICBkaXJuYW1lOiBmdW5jdGlvbiBkaXJuYW1lKGZpbGVwYXRoKSB7XG4gICAgcmV0dXJuIF9kaXJuYW1lKHRoaXMuc2VwLCBmaWxlcGF0aCk7XG4gIH0sXG4gIGlzQWJzb2x1dGU6IGZ1bmN0aW9uIGlzQWJzb2x1dGUoZmlsZXBhdGgpIHtcbiAgICByZXR1cm4gX2lzQWJzb2x1dGUoZmFsc2UsIGZpbGVwYXRoKTtcbiAgfSxcbiAgcmVsYXRpdmU6IGZ1bmN0aW9uIHJlbGF0aXZlKGZyb20sIHRvKSB7XG4gICAgcmV0dXJuIF9yZWxhdGl2ZSh0aGlzLnNlcCwgZnJvbSwgdG8pO1xuICB9LFxuICByZXNvbHZlOiBmdW5jdGlvbiByZXNvbHZlKCkge1xuICAgIGZvciAodmFyIF9sZW4yID0gYXJndW1lbnRzLmxlbmd0aCwgcGF0aHMgPSBuZXcgQXJyYXkoX2xlbjIpLCBfa2V5MiA9IDA7IF9rZXkyIDwgX2xlbjI7IF9rZXkyKyspIHtcbiAgICAgIHBhdGhzW19rZXkyXSA9IGFyZ3VtZW50c1tfa2V5Ml07XG4gICAgfVxuXG4gICAgcmV0dXJuIF9yZXNvbHZlKHRoaXMuc2VwLCBwYXRocyk7XG4gIH0sXG4gIHBhcnNlOiBmdW5jdGlvbiBwYXJzZShmaWxlcGF0aCkge1xuICAgIHJldHVybiBfcGFyc2UodGhpcy5zZXAsIGZpbGVwYXRoKTtcbiAgfSxcbiAgZm9ybWF0OiBmdW5jdGlvbiBmb3JtYXQocGF0aE9iamVjdCkge1xuICAgIHJldHVybiBfZm9ybWF0KHRoaXMuc2VwLCBwYXRoT2JqZWN0KTtcbiAgfSxcbiAgdG9OYW1lc3BhY2VkUGF0aDogdG9OYW1lc3BhY2VkUGF0aFxufTtcbnZhciBQb3NpeFBhdGggPSB7XG4gIHNlcDogJy8nLFxuICBkZWxpbWl0ZXI6ICc6JyxcbiAgYmFzZW5hbWU6IGZ1bmN0aW9uIGJhc2VuYW1lKGZpbGVwYXRoLCBleHQpIHtcbiAgICByZXR1cm4gX2Jhc2VuYW1lKHRoaXMuc2VwLCBmaWxlcGF0aCwgZXh0KTtcbiAgfSxcbiAgbm9ybWFsaXplOiBmdW5jdGlvbiBub3JtYWxpemUoZmlsZXBhdGgpIHtcbiAgICByZXR1cm4gX25vcm1hbGl6ZSh0aGlzLnNlcCwgZmlsZXBhdGgpO1xuICB9LFxuICBqb2luOiBmdW5jdGlvbiBqb2luKCkge1xuICAgIGZvciAodmFyIF9sZW4zID0gYXJndW1lbnRzLmxlbmd0aCwgcGF0aHMgPSBuZXcgQXJyYXkoX2xlbjMpLCBfa2V5MyA9IDA7IF9rZXkzIDwgX2xlbjM7IF9rZXkzKyspIHtcbiAgICAgIHBhdGhzW19rZXkzXSA9IGFyZ3VtZW50c1tfa2V5M107XG4gICAgfVxuXG4gICAgcmV0dXJuIF9qb2luKHRoaXMuc2VwLCBwYXRocyk7XG4gIH0sXG4gIGV4dG5hbWU6IGZ1bmN0aW9uIGV4dG5hbWUoZmlsZXBhdGgpIHtcbiAgICByZXR1cm4gX2V4dG5hbWUodGhpcy5zZXAsIGZpbGVwYXRoKTtcbiAgfSxcbiAgZGlybmFtZTogZnVuY3Rpb24gZGlybmFtZShmaWxlcGF0aCkge1xuICAgIHJldHVybiBfZGlybmFtZSh0aGlzLnNlcCwgZmlsZXBhdGgpO1xuICB9LFxuICBpc0Fic29sdXRlOiBmdW5jdGlvbiBpc0Fic29sdXRlKGZpbGVwYXRoKSB7XG4gICAgcmV0dXJuIF9pc0Fic29sdXRlKHRydWUsIGZpbGVwYXRoKTtcbiAgfSxcbiAgcmVsYXRpdmU6IGZ1bmN0aW9uIHJlbGF0aXZlKGZyb20sIHRvKSB7XG4gICAgcmV0dXJuIF9yZWxhdGl2ZSh0aGlzLnNlcCwgZnJvbSwgdG8pO1xuICB9LFxuICByZXNvbHZlOiBmdW5jdGlvbiByZXNvbHZlKCkge1xuICAgIGZvciAodmFyIF9sZW40ID0gYXJndW1lbnRzLmxlbmd0aCwgcGF0aHMgPSBuZXcgQXJyYXkoX2xlbjQpLCBfa2V5NCA9IDA7IF9rZXk0IDwgX2xlbjQ7IF9rZXk0KyspIHtcbiAgICAgIHBhdGhzW19rZXk0XSA9IGFyZ3VtZW50c1tfa2V5NF07XG4gICAgfVxuXG4gICAgcmV0dXJuIF9yZXNvbHZlKHRoaXMuc2VwLCBwYXRocyk7XG4gIH0sXG4gIHBhcnNlOiBmdW5jdGlvbiBwYXJzZShmaWxlcGF0aCkge1xuICAgIHJldHVybiBfcGFyc2UodGhpcy5zZXAsIGZpbGVwYXRoKTtcbiAgfSxcbiAgZm9ybWF0OiBmdW5jdGlvbiBmb3JtYXQocGF0aE9iamVjdCkge1xuICAgIHJldHVybiBfZm9ybWF0KHRoaXMuc2VwLCBwYXRoT2JqZWN0KTtcbiAgfSxcbiAgdG9OYW1lc3BhY2VkUGF0aDogZnVuY3Rpb24gdG9OYW1lc3BhY2VkUGF0aChmaWxlcGF0aCkge1xuICAgIHJldHVybiBmaWxlcGF0aDsgLy8gbm8tb3BcbiAgfVxufTtcbnZhciBwYXRoID0gaXNXaW4zMiA/IFdpbjMyUGF0aCA6IFBvc2l4UGF0aDtcbnBhdGgud2luMzIgPSBXaW4zMlBhdGg7XG5wYXRoLnBvc2l4ID0gUG9zaXhQYXRoO1xubW9kdWxlLmV4cG9ydHMgPSBwYXRoOyJdfQ==