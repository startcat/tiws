# tiws module

a native websocket module for titanium.

this is our first ios and android titanium native module trying to improve the developer flow (=> speed) using pure native xcode / android studio projects. the purpose of this doc is just try to enumerate the quirks we've found during the process.

## project organization

- `example/` example app
- `example/native_xcode` **xcode** native project
- `example/native` **android studio** native project (_TODO_: rename to eg `native_as`)
- `module/` titanium modules (ios + android)

## common tasks

- first of all, we need to build the module for each platform: `ti build -p iphone -b` and `ti build -p android-b`. this is going to generate some `c++` and `javascript` _binding_ to let any titanium app using the module how to to require and use it. once the module has been built, we need to unzip the zip inside `dist/` and copy the module titanium bundle for each platform inside the example `modules/` directory.

- to build the example app, we made some special `cli script` files which add additional actions after building the titanium app. those files are inside `example/`: `prepare_ios` and `prepare_android`.

- we also need to rebuild the module / the app whenever the module's api changes / we make some change to the example's `app.js`.

## ios

- if we're using **swift** in our module, **remember** to tell ti cli that the module is actually a **swift module**!

- for the xcode project, we first build the app using the script `example/prepare_ios` and then we copy all the contents inside `build/iphone` except for `build/`, `build-manifest.json` and `DerivedData/`. this is going to avoid titanium cli destroys whatever changes we do to the project whenever we rebuild the app.

- create a folder `alt + cmd + N` where we're going to add the javascript sources. then `alt + cmd + A` and browse **until** you see `native_xcode/js/` contents. then select all and add these files and folders using `Create folder references` option. this step was **the only required one** to make the app work using the plain generated xcode project!

next steps are required in order to being able to edit the module sources and run the app for testing!

- remove the references to our module inside the project. for `tiws` we needed to remove `CatStartTiws.framework` and `Starscream.framework` in `tiws-example (target) / General`, both in `Embedded Binaries` and `Linked Frameworks and Libraries` sections.

- drag our module xcode project (`module/ios/tiws.xcodeproj`) in our app xcode project (`example/native_xcode/tiws-example.xcodeproj`)

- add the embedded project module target (in our case is the dynamic framework `CatStartTiws.framework`) to `tiws-example (target) / Build Phases / Target Dependencies`

- finally in `tiws-example (target) / General / Embedded Binaries` we need to add the embedded xcode project products, in our case: `tiws.xcodeproj / Products / CatStartTiws.framework`. **also**, since our module has external framework dependency (`Starscream.framework` websocket 3rd party library), we need to add it to `Embedded Binaries` as well. in this case, we're going to use the `Add Other...` button and browse `module/ios/platform/Startscream.framework` (create references). this step **should** add the frameworks to the `General / Linked Frameworks and Libraries` and `Build Phases / Link Binary With Libraries` but... we know how xcode works sometimes.

## android

- in AS remember to disable **instant run**!

- we can edit the ti SDK version in `example/native/build.gradle` (`ext / tiHome` variable)

**each time we build the example app** edit the AndroidManifest in AS:

- remove:
  `<uses-sdk android:minSdkVersion="..." android:targetSdkVersion="..."/>`

- add to application level tag:
  `tools:replace="android:name,android:label"`

- i don't know how it's added, but we need to remove also this:
  `<meta-data android:name="com.google.android.gms.version" android:value="@integer/google_play_services_version"/>`

for apps using this module we need to add this in `tiapp.xml`:

```
<android xmlns:android="http://schemas.android.com/apk/res/android">
  <manifest xmlns:tools="http://schemas.android.com/tools"
    xmlns:android="http://schemas.android.com/apk/res/android">
    <application tools:replace="android:name,android:label" />
  </manifest>
</android>
```

- we keep ti's `android-support-multidex.jar` _special_ version in `example/native/app/libs/` since it's really messy to keep only that library with gradle.
