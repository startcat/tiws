package cat.start.tiws;

import android.os.Handler;
import android.util.Log;

import org.appcelerator.kroll.KrollDict;
import org.appcelerator.kroll.KrollFunction;
import org.appcelerator.kroll.KrollProxy;
import org.appcelerator.kroll.annotations.Kroll;

import com.koushikdutta.async.callback.CompletedCallback;
import com.koushikdutta.async.future.Future;
import com.koushikdutta.async.http.AsyncHttpClient;
import com.koushikdutta.async.http.AsyncHttpGet;
import com.koushikdutta.async.http.WebSocket;

@Kroll.proxy(creatableInModule = TiwsModule.class)
public class WebSocketProxy extends KrollProxy {
	private Future<WebSocket> client = null;

	private KrollFunction getCallback(String key, KrollDict options) {
		return options.containsKey(key) && options.get(key) instanceof KrollFunction ? (KrollFunction) options.get(key) : null;
	}

	@Override
    public void handleCreationDict(KrollDict options) {
		super.handleCreationDict(options);

        if (options == null) {
            throw new IllegalArgumentException("did not provide options");
        }

		if (!options.containsKeyAndNotNull("uri")) {
			throw new IllegalArgumentException("missing uri");
		}

		// request

		String uri = options.getString("uri");
		int connectionTimeout = !options.containsKey("connectionTimeout") ? 5 : options.getInt("connectionTimeout");

		AsyncHttpGet request = new AsyncHttpGet(uri.replace("ws://", "http://").replace("wss://", "https://"));
		request.setTimeout(connectionTimeout * 1000); // ms

		// other parameters

		final int pingIntervalMs = (
				!options.containsKey("pingInterval") ? 5 : options.getInt("pingInterval")
		) * 1000;

		final KrollFunction onOpen = getCallback("onOpen", options);
		final KrollFunction onError = getCallback("onError", options);
		final KrollFunction onMessage = getCallback("onMessage", options);
		final KrollFunction onClose = getCallback("onClose", options);

		// headers

		if (options.getKrollDict("headers") != null) {
			KrollDict headers = options.getKrollDict("headers");
			for (String key : headers.keySet()) {
				String value = options.getString(key);
				if (value != null) {
					request.addHeader(key, value);
				}
			}

		}

		// client

		client = AsyncHttpClient.getDefaultInstance().websocket(request, "", new AsyncHttpClient.WebSocketConnectCallback() {
			@Override
			public void onCompleted(Exception ex, final WebSocket webSocket) {
				if (ex != null) {
					if (onError != null) {
						KrollDict event = new KrollDict();
						event.put("error", Log.getStackTraceString(ex));
						onError.call(getKrollObject(), event);
					}
					return;
				}

				// ping / pong

				final Handler pingHandler = new Handler();
				final Runnable pingRunnable = new Runnable() {
					@Override
					public void run() {
						if (webSocket.isOpen()) {
							webSocket.ping("");
							pingHandler.postDelayed(this, pingIntervalMs);
						}
					}
				};
				pingHandler.postDelayed(pingRunnable, pingIntervalMs);

				webSocket.setPingCallback(new WebSocket.PingCallback() {
					@Override
					public void onPingReceived(String s) {
						webSocket.pong(s);
					}
				});

				// callbacks

				if (onOpen != null) {
					KrollDict event = new KrollDict();
					onOpen.call(getKrollObject(), event);
				}

				if (onMessage != null) {
					webSocket.setStringCallback(new WebSocket.StringCallback() {
						@Override
						public void onStringAvailable(String s) {
							KrollDict event = new KrollDict();
							event.put("data", s);
							onMessage.call(getKrollObject(), event);
						}
					});

				}

				webSocket.setClosedCallback(new CompletedCallback() {
					@Override
					public void onCompleted(Exception e) {
						pingHandler.removeCallbacks(pingRunnable);
						if (onClose != null) {
							KrollDict event = new KrollDict();
							if (e != null) {
								event.put("error", Log.getStackTraceString(e));
							}
							onClose.call(getKrollObject(), event);
						}
					}
				});
			}
		});

	}

	// Methods

	@Kroll.method
	public void close() {
		if (client.isDone()) {
			client.tryGet().close();
			return;
		}
		client.cancel(true);
	}


	@Kroll.method
	public void send(String message) {
		if (!client.isDone()) {
			return;
		}
		client.tryGet().send(message);
	}

	@Kroll.method
	public void ping() {
		if (!client.isDone()) {
			return;
		}
		client.tryGet().ping("");
	}
}
