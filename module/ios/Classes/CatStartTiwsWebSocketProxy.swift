//
//  WebSocketProxy.swift
//  tiws
//
//  Created by yellow on 02/08/2019.
//

import Foundation
import TitaniumKit
import Starscream

// https://docs.axway.com/bundle/Titanium_SDK_allOS_en/page/ios_module_quick_start.html
// https://docs.axway.com/bundle/Titanium_SDK_allOS_en/page/using_swift_to_build_native_modules_in_titanium.html
// https://github.com/appcelerator-modules/ti.moddevguide/blob/master/ios/Classes/TiModdevguideLifeCycleProxy.m
// https://github.com/hansemannn/titanium-pspdfkit-ios/blob/master/Classes/TiPspdfkitModule.swift
// https://github.com/daltoniam/Starscream

@objc(CatStartTiwsWebSocketProxy)
class CatStartTiwsWebSocketProxy: TiProxy {
    
    var socket: WebSocket?
    var pingTimer: Timer?
    var backgroundTask: UIBackgroundTaskIdentifier = .invalid
    
    override func _init(withProperties _properties: [AnyHashable : Any]!) {
        super._init(withProperties: _properties)
        
        // needed properties check
        
        guard let properties = _properties else {
            fatalError("did not provide options")
        }
        
        guard let uri = properties["uri"] as? String else {
            fatalError("missing uri")
        }
        
        // request
        
        var request = URLRequest(url: URL(string: uri)!)
        
        var connectionTimeout: TimeInterval = 5
        if let pConnectionTimeout = properties["connectionTimeout"] as? TimeInterval {
            connectionTimeout = pConnectionTimeout
        }
        request.timeoutInterval = connectionTimeout
        
        // headers
        
        if let headers = properties["headers"] as? [String: String] {
            for (key, value) in headers {
                request.addValue(value, forHTTPHeaderField: key)
            }
        }
        
        // socket
        
        var didOpen = false
        socket = WebSocket(request: request)

        socket!.onConnect = {
            didOpen = true
            
            var pingInterval: TimeInterval = 10.0
            if let pPingInterval = properties["pingInterval"] as? TimeInterval {
                pingInterval = pPingInterval
            }

            self.pingTimer = Timer.scheduledTimer(withTimeInterval: pingInterval, repeats: true) { [weak self] _ in
                if let ws = self?.socket {
                    if ws.isConnected {
                        ws.write(ping: Data())
                    }
                }
            }
            
            if let onOpen = properties["onOpen"] as? KrollCallback {
                onOpen.call([], thisObject: nil)
            }
        }
        
        socket!.onText = { (text: String) in
            if let onMessage = properties["onMessage"] as? KrollCallback {
                onMessage.call([["data": text]], thisObject: nil)
            }
        }

        socket!.onDisconnect = { (error: Error?) in
            if !didOpen {
                if let onError = properties["onError"] as? KrollCallback {
                    if let err = error {
                        onError.call([["error": err.localizedDescription]], thisObject: nil)
                    }
                    else {
                        onError.call([], thisObject: nil)
                    }
                }
            }
            else {
                if let onClose = properties["onClose"] as? KrollCallback {
                    if let err = error {
                        onClose.call([["error": err.localizedDescription]], thisObject: nil)
                    }
                    else {
                        onClose.call([], thisObject: nil)
                    }
                }
            }
            
            self.pingTimer?.invalidate()
            self.pingTimer = nil
            
            self.socket = nil
            
            self.endEnsureBackgroundTime()
        }

        socket!.connect()
    }
    
    @objc(send:)
    func send(args: [Any]?) {
        if let ws = socket, let message = args?.first as? String {
            ws.write(string: message)
        }
    }
    
    @objc(ping:)
    func ping(args: [Any]?) {
        socket?.write(ping: Data())
    }
    
    @objc(close:)
    func close(args: [Any]?) {
        if let ws = socket {
            // disconnecting could take a while, so we may need some time if we're disconnecting
            // and user's moving into background. if not, socket may become unstable and the OS
            // could kill our app
            
            beginEnsureBackgroundTime()
            
            let time = min(UIApplication.shared.backgroundTimeRemaining, 5)
            
            ws.disconnect(forceTimeout: time, closeCode: CloseCode.normal.rawValue)
        }
    }
    
    // background
    
    func beginEnsureBackgroundTime() {
        if backgroundTask == .invalid {
            backgroundTask = UIApplication.shared.beginBackgroundTask { [weak self] in
                self?.endEnsureBackgroundTime()
            }
        }
    }
    
    func endEnsureBackgroundTime() {
        if backgroundTask != .invalid {
            if let ws = socket {
                ws.disconnect(forceTimeout: 0, closeCode: CloseCode.normal.rawValue)
                self.socket = nil
            }

            UIApplication.shared.endBackgroundTask(backgroundTask)
            backgroundTask = .invalid
        }
    }
}
