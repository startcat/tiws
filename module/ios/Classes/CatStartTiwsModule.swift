//
//  CatStartTiwsModule.swift
//  tiws
//
//  Created by jordi domenech
//  Copyright (c) 2019 Your Company. All rights reserved.
//

import UIKit
import TitaniumKit

/**
 
 Titanium Swift Module Requirements
 ---
 
 1. Use the @objc annotation to expose your class to Objective-C (used by the Titanium core)
 2. Use the @objc annotation to expose your method to Objective-C as well.
 3. Method arguments always have the "[Any]" type, specifying a various number of arguments.
 Unwrap them like you would do in Swift, e.g. "guard let arguments = arguments, let message = arguments.first"
 4. You can use any public Titanium API like before, e.g. TiUtils. Remember the type safety of Swift, like Int vs Int32
 and NSString vs. String.
 
 */

@objc(CatStartTiwsModule)
class CatStartTiwsModule: TiModule {

  func moduleGUID() -> String {
    return "cea5aa9b-f929-436a-a177-48ebe941706a"
  }
  
  override func moduleId() -> String! {
    return "cat.start.tiws"
  }

  override func startup() {
    super.startup()
    debugPrint("[DEBUG] \(self) loaded")
  }
}
