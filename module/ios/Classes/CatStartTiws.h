//
//  CatStartTiws.h
//  tiws
//
//  Created by jordi domenech
//  Copyright (c) 2019 Your Company. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CatStartTiws.
FOUNDATION_EXPORT double CatStartTiwsVersionNumber;

//! Project version string for CatStartTiws.
FOUNDATION_EXPORT const unsigned char CatStartTiwsVersionString[];

#import "CatStartTiwsModuleAssets.h"
